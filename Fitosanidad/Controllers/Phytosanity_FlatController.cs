﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Resources;
using System.Data;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Net;
using Planner.Model;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class Phytosanity_FlatController : BaseController
    {
        SIEMBRAS_PLANNEREntities _SIEMBRAS_PLANNEREntities = null;
        SIEMBRASEntities _SIEMBRASEntities = null;

        public ActionResult Index()
        {
            string currentWeek = BaseController.GetWeek(DateTime.Now);

            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            ViewBag.TblInd = 0;

            //Week
            SelectList lstWeek = null;
            var aListWeek = _SIEMBRAS_PLANNEREntities.Semanas.Where(n => n.Anio >= 2018).Select(x=> new { ID = x.ID, Name = x.Anio + " - " + x.Nombre}).ToList();
            var intSelected = aListWeek.Find(x => x.Name == currentWeek);
            lstWeek = new SelectList(aListWeek, "ID", "Name".Trim().ToUpper(), intSelected.ID);
            ViewBag.lstWeek = lstWeek;

            //BusinessUnit
            SelectList lstBusinessUnit = null;
            var aList = _SIEMBRAS_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            lstBusinessUnit = new SelectList(aList, "ID", "Name".Trim().ToUpper());
            ViewData["lstBusinessUnit"] = lstBusinessUnit;

            var a  = lstBusinessUnit.First().Value;
            getGreenHousebyBusinessUnit(a);

            getPhytosanity_Flat(intSelected.ID, Convert.ToInt32(lstBusinessUnit.FirstOrDefault().Value), ViewBag.GreenhouseId);

            setPhytosanity_Flat(intSelected.ID, Convert.ToInt32(lstBusinessUnit.FirstOrDefault().Value), ViewBag.GreenhouseId, 0);
            setPhytosanity_Flat(intSelected.ID, Convert.ToInt32(lstBusinessUnit.FirstOrDefault().Value), ViewBag.GreenhouseId, 1);
            setPhytosanity_Flat(intSelected.ID, Convert.ToInt32(lstBusinessUnit.FirstOrDefault().Value), ViewBag.GreenhouseId, 6);
            setPhytosanity_Flat(intSelected.ID, Convert.ToInt32(lstBusinessUnit.FirstOrDefault().Value), ViewBag.GreenhouseId, 7);
            setPhytosanity_Flat(intSelected.ID, Convert.ToInt32(lstBusinessUnit.FirstOrDefault().Value), ViewBag.GreenhouseId, 8);

            getPhytosanity_FlatFooter(intSelected.ID, Convert.ToInt32(lstBusinessUnit.FirstOrDefault().Value), ViewBag.GreenhouseId);

            return View();
        }

        public int getPhytosanity_Flat(int prmIDSemana, int prmIDUnidadesNegocios, int prmIDInvernadero)
        {
            _SIEMBRASEntities = new SIEMBRASEntities();
            ViewBag.TblInd = 0;

            try
            {
                string strQry = "SELECT DISTINCT IDBAS_TablaCama FROM Camas WHERE IDInvernaderos = @prmIDInvernadero";

                using (SqlConnection connection = (SqlConnection)_SIEMBRASEntities.Database.Connection)
                {
                    SqlCommand command = new SqlCommand(strQry, connection);
                    command.Parameters.AddWithValue("@prmIDInvernadero", prmIDInvernadero);
                    connection.Open();
                    SqlDataReader dr = command.ExecuteReader();
                    try
                    {
                        while (dr.Read())
                        {
                            ViewBag.TblInd++;
                        }
                    }
                    finally
                    {
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ViewBag.TblInd;
        }

        public ActionResult setPhytosanity_Flat(int prmIDSemana, int prmIDUnidadesNegocios, int prmIDInvernadero, int prmTipoConsulta)
        {
            try
            {
                if (prmTipoConsulta == 0)
                {
                    ViewData["Phytosanity_FlatListPar"] = getCargaInformacionPlanoMonitoreo(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, prmTipoConsulta, true);
                    return PartialView("Phytosanity_FlatListPar");
                }
                if (prmTipoConsulta == 1)
                {
                    ViewData["Phytosanity_FlatListImpar"] = getCargaInformacionPlanoMonitoreo(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, prmTipoConsulta, true);
                    return PartialView("Phytosanity_FlatListImpar");
                }

                if (prmTipoConsulta == 6)
                {
                    ViewData["Phytosanity_FlatListA"] = getCargaInformacionPlanoMonitoreo(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, prmTipoConsulta, true);
                    return PartialView("Phytosanity_FlatListA");
                }
                if (prmTipoConsulta == 7)
                {
                    ViewData["Phytosanity_FlatListB"] = getCargaInformacionPlanoMonitoreo(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, prmTipoConsulta, true);
                    return PartialView("Phytosanity_FlatListB");
                }
                if (prmTipoConsulta == 8)
                {
                    ViewData["Phytosanity_FlatListC"] = getCargaInformacionPlanoMonitoreo(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, prmTipoConsulta, true);
                    return PartialView("Phytosanity_FlatListC");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            ViewData["Phytosanity_FlatListPar"] = getCargaInformacionPlanoMonitoreo(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 0, true);
            return PartialView("Phytosanity_FlatListPar");
        }

        public ActionResult getGreenHousebyBusinessUnit(string BusinessUnitID)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            SelectList lstGreenhouse = null;
            var aList = _SIEMBRAS_PLANNEREntities.fn_getGreenHousebyBusinessUnit(Convert.ToInt32(BusinessUnitID)).OrderBy(a => a.Orden).ToList();
            lstGreenhouse = new SelectList(aList, "ID", "Codigo".Trim().ToUpper());
            ViewBag.GreenhouseId = Convert.ToInt32(lstGreenhouse.FirstOrDefault().Value);
            ViewData["lstGreenhouse"] = lstGreenhouse;

            return Json(lstGreenhouse, JsonRequestBehavior.AllowGet);
        }

        public List<PlanoMonitoreo> getCargaInformacionPlanoMonitoreo(int prmIDSemana, int prmIDUnidadesNegocios, int prmIDInvernadero, int prmTipoConsulta, bool booIndColor)
        {
            _SIEMBRASEntities = new SIEMBRASEntities();

            SqlConnection entityConnection = (SqlConnection)_SIEMBRASEntities.Database.Connection;
            SqlConnection cnn = entityConnection;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "CargaInformacionPlanoMonitoreo";

            SqlParameter _prmIDSemana = new SqlParameter("@prmIDSemana", SqlDbType.Int);
            SqlParameter _prmIDUnidadesNegocios = new SqlParameter("@prmIDUnidadesNegocios", SqlDbType.Int);
            SqlParameter _prmIDInvernadero = new SqlParameter("@prmIDInvernadero", SqlDbType.Int);
            SqlParameter _prmTipoConsulta = new SqlParameter("@prmTipoConsulta", SqlDbType.Int);

            _prmIDSemana.Value = prmIDSemana;
            _prmIDUnidadesNegocios.Value = prmIDUnidadesNegocios;
            _prmIDInvernadero.Value = prmIDInvernadero;
            _prmTipoConsulta.Value = prmTipoConsulta;

            cmd.Parameters.Add(_prmIDSemana);
            cmd.Parameters.Add(_prmIDUnidadesNegocios);
            cmd.Parameters.Add(_prmIDInvernadero);
            cmd.Parameters.Add(_prmTipoConsulta);

            cnn.Open();

            List<PlanoMonitoreo> _PlanoMonitoreoLst = new List<PlanoMonitoreo>();

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    PlanoMonitoreo _PlanoMonitoreo = new PlanoMonitoreo();

                    _PlanoMonitoreo.Cama = Convert.ToInt32(dr[0]);
                    _PlanoMonitoreo.IDCama = Convert.ToInt32(dr[1]);
                    _PlanoMonitoreo.Producto = dr[2].ToString();
                    if (booIndColor)
                    {
                        _PlanoMonitoreo.Cuadro1 = getPlagaColor(dr[3].ToString());
                        _PlanoMonitoreo.Cuadro2 = getPlagaColor(dr[4].ToString());
                        _PlanoMonitoreo.Cuadro3 = getPlagaColor(dr[5].ToString());
                        _PlanoMonitoreo.Cuadro4 = getPlagaColor(dr[6].ToString());
                        _PlanoMonitoreo.Cuadro5 = getPlagaColor(dr[7].ToString());
                    }
                    else
                    {
                        _PlanoMonitoreo.Cuadro1 = dr[3].ToString();
                        _PlanoMonitoreo.Cuadro2 = dr[4].ToString();
                        _PlanoMonitoreo.Cuadro3 = dr[5].ToString();
                        _PlanoMonitoreo.Cuadro4 = dr[6].ToString();
                        _PlanoMonitoreo.Cuadro5 = dr[7].ToString();
                    }

                    _PlanoMonitoreoLst.Add(_PlanoMonitoreo);
                }

                dr.Close();
            }

            cnn.Close();

            return _PlanoMonitoreoLst;
        }

        public ActionResult getPhytosanity_FlatHeader(int prmIDSemana, int prmIDUnidadesNegocios, int prmIDInvernadero)
        {
            _SIEMBRASEntities = new SIEMBRASEntities();
            try
            {
                SqlConnection entityConnection = (SqlConnection)_SIEMBRASEntities.Database.Connection;
                SqlConnection cnn = entityConnection;
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cnn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "CargaInformacionPlanoMonitoreo";

                SqlParameter _prmIDSemana = new SqlParameter("@prmIDSemana", SqlDbType.Int);
                SqlParameter _prmIDUnidadesNegocios = new SqlParameter("@prmIDUnidadesNegocios", SqlDbType.Int);
                SqlParameter _prmIDInvernadero = new SqlParameter("@prmIDInvernadero", SqlDbType.Int);
                SqlParameter _prmTipoConsulta = new SqlParameter("@prmTipoConsulta", SqlDbType.Int);

                _prmIDSemana.Value = prmIDSemana;
                _prmIDUnidadesNegocios.Value = prmIDUnidadesNegocios;
                _prmIDInvernadero.Value = prmIDInvernadero;
                _prmTipoConsulta.Value = 2;

                cmd.Parameters.Add(_prmIDSemana);
                cmd.Parameters.Add(_prmIDUnidadesNegocios);
                cmd.Parameters.Add(_prmIDInvernadero);
                cmd.Parameters.Add(_prmTipoConsulta);

                cnn.Open();

                string[] _PlanoMonitoreoHeader = new string[5];

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        _PlanoMonitoreoHeader[0] = dr[0].ToString();
                        _PlanoMonitoreoHeader[1] = dr[1].ToString();
                        _PlanoMonitoreoHeader[2] = dr[2].ToString();
                        _PlanoMonitoreoHeader[3] = dr[3].ToString();
                        _PlanoMonitoreoHeader[4] = dr[4].ToString();
                    }

                    dr.Close();
                }

                cnn.Close();

                getPhytosanity_Flat(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero);


                return Json(_PlanoMonitoreoHeader, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult getPhytosanity_FlatFooter(int prmIDSemana, int prmIDUnidadesNegocios, int prmIDInvernadero)
        {
            try
            {
                _SIEMBRASEntities = new SIEMBRASEntities();

                SqlConnection entityConnection = (SqlConnection)_SIEMBRASEntities.Database.Connection;
                SqlConnection cnn = entityConnection;
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cnn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "CargaInformacionPlanoMonitoreo";

                SqlParameter _prmIDSemana = new SqlParameter("@prmIDSemana", SqlDbType.Int);
                SqlParameter _prmIDUnidadesNegocios = new SqlParameter("@prmIDUnidadesNegocios", SqlDbType.Int);
                SqlParameter _prmIDInvernadero = new SqlParameter("@prmIDInvernadero", SqlDbType.Int);
                SqlParameter _prmTipoConsulta = new SqlParameter("@prmTipoConsulta", SqlDbType.Int);

                _prmIDSemana.Value = prmIDSemana;
                _prmIDUnidadesNegocios.Value = prmIDUnidadesNegocios;
                _prmIDInvernadero.Value = prmIDInvernadero;
                _prmTipoConsulta.Value = 3;

                cmd.Parameters.Add(_prmIDSemana);
                cmd.Parameters.Add(_prmIDUnidadesNegocios);
                cmd.Parameters.Add(_prmIDInvernadero);
                cmd.Parameters.Add(_prmTipoConsulta);

                cnn.Open();

                PlanoMonitoreoFooter _PlanoMonitoreoFooterDataTable = new PlanoMonitoreoFooter();

                DataTable dtPhytosanity_FlatListFooter = new DataTable();
                dtPhytosanity_FlatListFooter.Columns.Add(Resources.Resources.BlancoBiologico, Type.GetType("System.String"));
                dtPhytosanity_FlatListFooter.Columns.Add(Resources.Resources.Camas, Type.GetType("System.Int32"));
                dtPhytosanity_FlatListFooter.Columns.Add(Resources.Resources.PorcentajeIncidencia, Type.GetType("System.Decimal"));
                dtPhytosanity_FlatListFooter.Columns.Add(Resources.Resources.CuadrosAfectados, Type.GetType("System.Int32"));
                dtPhytosanity_FlatListFooter.Columns.Add(Resources.Resources.PorcentajeSeveridad, Type.GetType("System.Decimal"));

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtPhytosanity_FlatListFooter.Rows.Add(new object[] { dr[2].ToString(), Convert.ToInt32(dr[0]), Convert.ToDecimal(dr[3]), Convert.ToInt32(dr[1]), Convert.ToDecimal(dr[4]) });
                    }

                    dr.Close();
                }

                cnn.Close();

                dtPhytosanity_FlatListFooter = HelperController.GetPivotTable(dtPhytosanity_FlatListFooter, Resources.Resources.BlancoBiologico);

                foreach (DataColumn column in dtPhytosanity_FlatListFooter.Columns)
                {
                    if(column.ToString() != Resources.Resources.BlancoBiologico)
                    {
                        dtPhytosanity_FlatListFooter.Columns[column.ToString()].ColumnName = getPlagaColor(column.ToString());
                    }
                }

                ViewData["Phytosanity_FlatListFooter"] = dtPhytosanity_FlatListFooter;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PartialView("Phytosanity_FlatFooter");
        }

        public string getPlagaColor(string strPlaga)
        {
            _SIEMBRASEntities = new SIEMBRASEntities();

            string[] arrPlagas = strPlaga.Split(' ');
            string strPlagasColor = "";
            try
            {
                foreach (string Plaga in arrPlagas)
                {
                    if(Plaga != string.Empty)
                    {
                        string _Plaga = new String(Plaga.Where(Char.IsLetter).ToArray());
                        string _Tercio = new String(Plaga.Where(Char.IsDigit).ToArray());
                        strPlagasColor += "<span style='font-weight:bolder; color:" + _SIEMBRASEntities.Plagas.Where(x => x.Abrr == _Plaga).FirstOrDefault().ColorIdentificacion + "'>" + _Plaga + _Tercio + "</span><br>";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strPlagasColor.Trim();
        }

        public ActionResult getPhytosanity_FlatPdf(int prmIDSemana, int prmIDUnidadesNegocios, int prmIDInvernadero)
        {
            try
            {
                string strFileName = Resources.Resources.PlanoDeMonitoreo + ".pdf";
                strFileName = strFileName.Replace(" ", "");
                string strPath = HelperController.VerifyExportEnvironment(strFileName);

                Document pdfDoc = new Document(PageSize.LETTER_LANDSCAPE.Rotate(), 2, 2, 2, 2);

                string FONTNAME = "segoeui.ttf";
                string fg = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), FONTNAME);
                Font textFont = new Font(BaseFont.CreateFont(fg, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 12.0f, Font.NORMAL, BaseColor.BLACK);

                PdfWriter.GetInstance(pdfDoc, new FileStream(strPath, FileMode.Create));

                pdfDoc.Open();

                pdfDoc = getPhytosaniy_Flat_PdfHeader(pdfDoc, prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero);

                pdfDoc = getPhytosaniy_Flat_PdfContent(pdfDoc, prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero);
                pdfDoc.Add(Chunk.NEWLINE);

                pdfDoc.Close();

                byte[] fileBytes = System.IO.File.ReadAllBytes(strPath);
                string fileName = strFileName;
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Document getPhytosaniy_Flat_PdfHeader(Document pdfDoc, int prmIDSemana, int prmIDUnidadesNegocios, int prmIDInvernadero)
        {
            //Title
            Font fntTitle = FontFactory.GetFont("Arial", 12);
            Paragraph prgTitle = new Paragraph(Resources.Resources.PlanoDeMonitoreo, fntTitle);
            prgTitle.Alignment = Element.ALIGN_CENTER;
            //Title

            //Table Header Font
            Font fntHeader = FontFactory.GetFont("Arial", 10);
            //Table Header Font

            //Table Content Font
            Font fntContent = FontFactory.GetFont("Arial", 10);
            //Table Content Font

            PdfPTable tblHeader = new PdfPTable(5);

            PdfPCell cell = new PdfPCell(prgTitle);
            cell.Colspan = 5;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = new iTextSharp.text.BaseColor(238, 238, 238);
            tblHeader.AddCell(cell);

            cell = new PdfPCell(new Phrase(Resources.Resources.Semana, fntContent));
            tblHeader.AddCell(cell);

            cell = new PdfPCell(new Phrase(Resources.Resources.Camas, fntContent));
            tblHeader.AddCell(cell);

            cell = new PdfPCell(new Phrase(Resources.Resources.Siembra, fntContent));
            tblHeader.AddCell(cell);

            cell = new PdfPCell(new Phrase(Resources.Resources.Cultivo, fntContent));
            tblHeader.AddCell(cell);

            cell = new PdfPCell(new Phrase(Resources.Resources.Bloque, fntContent));
            tblHeader.AddCell(cell);

            _SIEMBRASEntities = new SIEMBRASEntities();

            SqlConnection entityConnection = (SqlConnection)_SIEMBRASEntities.Database.Connection;
            SqlConnection cnn = entityConnection;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "CargaInformacionPlanoMonitoreo";

            SqlParameter _prmIDSemana = new SqlParameter("@prmIDSemana", SqlDbType.Int);
            SqlParameter _prmIDUnidadesNegocios = new SqlParameter("@prmIDUnidadesNegocios", SqlDbType.Int);
            SqlParameter _prmIDInvernadero = new SqlParameter("@prmIDInvernadero", SqlDbType.Int);
            SqlParameter _prmTipoConsulta = new SqlParameter("@prmTipoConsulta", SqlDbType.Int);

            _prmIDSemana.Value = prmIDSemana;
            _prmIDUnidadesNegocios.Value = prmIDUnidadesNegocios;
            _prmIDInvernadero.Value = prmIDInvernadero;
            _prmTipoConsulta.Value = 2;

            cmd.Parameters.Add(_prmIDSemana);
            cmd.Parameters.Add(_prmIDUnidadesNegocios);
            cmd.Parameters.Add(_prmIDInvernadero);
            cmd.Parameters.Add(_prmTipoConsulta);

            cnn.Open();

            string[] _PlanoMonitoreoHeader = new string[5];

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    cell = new PdfPCell(new Phrase(dr[0].ToString(), fntContent));
                    tblHeader.AddCell(cell);

                    cell = new PdfPCell(new Phrase(dr[2].ToString(), fntContent));
                    tblHeader.AddCell(cell);

                    cell = new PdfPCell(new Phrase(dr[4].ToString(), fntContent));
                    tblHeader.AddCell(cell);

                    cell = new PdfPCell(new Phrase(dr[4].ToString(), fntContent));
                    tblHeader.AddCell(cell);

                    cell = new PdfPCell(new Phrase(dr[1].ToString(), fntContent));
                    tblHeader.AddCell(cell);
                }

                dr.Close();
            }

            pdfDoc.Add(tblHeader);

            return pdfDoc;
        }

        public Document getPhytosaniy_Flat_PdfContent(Document pdfDoc, int prmIDSemana, int prmIDUnidadesNegocios, int prmIDInvernadero)
        {
            int TblInd;
            int intRowTot;
            DataTable PlanoMonitoreoDt;

            //Table Content Font
            Font fntContent = FontFactory.GetFont("Arial", 6);
            //Table Content Font

            TblInd = getPhytosanity_Flat(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero);

            if (TblInd == 1)
            {
                PlanoMonitoreoDt = new DataTable();
                PlanoMonitoreoDt.Columns.Add("A" + Resources.Resources.Producto, Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("A" + Resources.Resources.Cuadro + "5", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("A" + Resources.Resources.Cuadro + "4", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("A" + Resources.Resources.Cuadro + "3", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("A" + Resources.Resources.Cuadro + "2", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("A" + Resources.Resources.Cuadro + "1", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("A" + Resources.Resources.Cama, Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("B" + Resources.Resources.Cama, Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("B" + Resources.Resources.Cuadro + "1", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("B" + Resources.Resources.Cuadro + "2", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("B" + Resources.Resources.Cuadro + "3", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("B" + Resources.Resources.Cuadro + "4", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("B" + Resources.Resources.Cuadro + "5", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("B" + Resources.Resources.Producto, Type.GetType("System.String"));

                List<PlanoMonitoreo> PlanoMonitoreoLstA = getCargaInformacionPlanoMonitoreo(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 0, false);
                List<PlanoMonitoreo> PlanoMonitoreoLstB = getCargaInformacionPlanoMonitoreo(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 1, false);

                intRowTot = PlanoMonitoreoLstA.Count >= PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstA.Count : PlanoMonitoreoLstB.Count;

                for (int i = 0; i < intRowTot; i++)
                {
                    PlanoMonitoreoDt.Rows.Add(
                        ( i < PlanoMonitoreoLstA.Count ? PlanoMonitoreoLstA[i].Producto : "")
                        , (i < PlanoMonitoreoLstA.Count ? PlanoMonitoreoLstA[i].Cuadro5 : "")
                        , (i < PlanoMonitoreoLstA.Count ? PlanoMonitoreoLstA[i].Cuadro4 : "")
                        , (i < PlanoMonitoreoLstA.Count ? PlanoMonitoreoLstA[i].Cuadro3 : "")
                        , (i < PlanoMonitoreoLstA.Count ? PlanoMonitoreoLstA[i].Cuadro2 : "")
                        , (i < PlanoMonitoreoLstA.Count ? PlanoMonitoreoLstA[i].Cuadro1 : "")
                        , (i < PlanoMonitoreoLstA.Count ? PlanoMonitoreoLstA[i].Cama.ToString() : "")
                        , (i < PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstB[i].Cama.ToString() : "")
                        , (i < PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstB[i].Cuadro1 : "")
                        , (i < PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstB[i].Cuadro2 : "")
                        , (i < PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstB[i].Cuadro3 : "")
                        , (i < PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstB[i].Cuadro4 : "")
                        , (i < PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstB[i].Cuadro5 : "")
                        , (i < PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstB[i].Producto : "")
                        );
                }

                pdfDoc = HelperController.DataTable2Pdf(PlanoMonitoreoDt, pdfDoc, fntContent, PlanoMonitoreoDt.Columns.Count, 50);
            }
            else
            {
                PlanoMonitoreoDt = new DataTable();
                PlanoMonitoreoDt.Columns.Add("A" + Resources.Resources.Producto, Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("A" + Resources.Resources.Cuadro + "5", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("A" + Resources.Resources.Cuadro + "4", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("A" + Resources.Resources.Cuadro + "3", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("A" + Resources.Resources.Cuadro + "2", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("A" + Resources.Resources.Cuadro + "1", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("A" + Resources.Resources.Cama, Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("B" + Resources.Resources.Cama, Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("B" + Resources.Resources.Cuadro + "1", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("B" + Resources.Resources.Cuadro + "2", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("B" + Resources.Resources.Cuadro + "3", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("B" + Resources.Resources.Cuadro + "4", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("B" + Resources.Resources.Cuadro + "5", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("B" + Resources.Resources.Producto, Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("C" + Resources.Resources.Cama, Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("C" + Resources.Resources.Cuadro + "1", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("C" + Resources.Resources.Cuadro + "2", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("C" + Resources.Resources.Cuadro + "3", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("C" + Resources.Resources.Cuadro + "4", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("C" + Resources.Resources.Cuadro + "5", Type.GetType("System.String"));
                PlanoMonitoreoDt.Columns.Add("C" + Resources.Resources.Producto, Type.GetType("System.String"));

                List<PlanoMonitoreo> PlanoMonitoreoLstA = getCargaInformacionPlanoMonitoreo(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 6, false);
                List<PlanoMonitoreo> PlanoMonitoreoLstB = getCargaInformacionPlanoMonitoreo(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 7, false);
                List<PlanoMonitoreo> PlanoMonitoreoLstC = getCargaInformacionPlanoMonitoreo(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 8, false);

                intRowTot = PlanoMonitoreoLstA.Count >= PlanoMonitoreoLstB.Count && PlanoMonitoreoLstA.Count >= PlanoMonitoreoLstC.Count ? PlanoMonitoreoLstA.Count : PlanoMonitoreoLstB.Count >= PlanoMonitoreoLstA.Count && PlanoMonitoreoLstB.Count >= PlanoMonitoreoLstC.Count ? PlanoMonitoreoLstB.Count : PlanoMonitoreoLstC.Count;

                for (int i = 0; i < intRowTot; i++)
                {
                    PlanoMonitoreoDt.Rows.Add(
                        (i < PlanoMonitoreoLstA.Count ? PlanoMonitoreoLstA[i].Producto : "")
                        , (i < PlanoMonitoreoLstA.Count ? PlanoMonitoreoLstA[i].Cuadro5 : "")
                        , (i < PlanoMonitoreoLstA.Count ? PlanoMonitoreoLstA[i].Cuadro4 : "")
                        , (i < PlanoMonitoreoLstA.Count ? PlanoMonitoreoLstA[i].Cuadro3 : "")
                        , (i < PlanoMonitoreoLstA.Count ? PlanoMonitoreoLstA[i].Cuadro2 : "")
                        , (i < PlanoMonitoreoLstA.Count ? PlanoMonitoreoLstA[i].Cuadro1 : "")
                        , (i < PlanoMonitoreoLstA.Count ? PlanoMonitoreoLstA[i].Cama.ToString() : "")
                        , (i < PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstB[i].Cama.ToString() : "")
                        , (i < PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstB[i].Cuadro1 : "")
                        , (i < PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstB[i].Cuadro2 : "")
                        , (i < PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstB[i].Cuadro3 : "")
                        , (i < PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstB[i].Cuadro4 : "")
                        , (i < PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstB[i].Cuadro5 : "")
                        , (i < PlanoMonitoreoLstB.Count ? PlanoMonitoreoLstB[i].Producto : "")
                        , (i < PlanoMonitoreoLstC.Count ? PlanoMonitoreoLstC[i].Cama.ToString() : "")
                        , (i < PlanoMonitoreoLstC.Count ? PlanoMonitoreoLstC[i].Cuadro1 : "")
                        , (i < PlanoMonitoreoLstC.Count ? PlanoMonitoreoLstC[i].Cuadro2 : "")
                        , (i < PlanoMonitoreoLstC.Count ? PlanoMonitoreoLstC[i].Cuadro3 : "")
                        , (i < PlanoMonitoreoLstC.Count ? PlanoMonitoreoLstC[i].Cuadro4 : "")
                        , (i < PlanoMonitoreoLstC.Count ? PlanoMonitoreoLstC[i].Cuadro5 : "")
                        , (i < PlanoMonitoreoLstC.Count ? PlanoMonitoreoLstC[i].Producto : "")
                        );
                }

                pdfDoc = HelperController.DataTable2Pdf(PlanoMonitoreoDt, pdfDoc, fntContent, PlanoMonitoreoDt.Columns.Count, 50);
            }

            return pdfDoc;
        }

    }
}