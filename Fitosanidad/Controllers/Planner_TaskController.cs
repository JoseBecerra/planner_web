﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Planner.Model;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class Planner_TaskController : Controller
    {
        SIEMBRAS_PLANNEREntities SIEMBRAS_PLANNEREntities = null;
        // GET: Planner_Task
        public ActionResult Index()
        {
            SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            ViewBag.Planner_TaskList = SIEMBRAS_PLANNEREntities.sp_ReportTaskExecution(Convert.ToDateTime("02/01/2019"), Convert.ToDateTime("3/4/2019")).ToList();

            return View(SIEMBRAS_PLANNEREntities);
        }

        public ActionResult GetData(DateTime startDate, DateTime endDate)
        {
            SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            ViewBag.Planner_TaskList = SIEMBRAS_PLANNEREntities.sp_ReportTaskExecution(startDate, endDate).ToList();

            return PartialView("Planner_TaskList");
        }
    }
}