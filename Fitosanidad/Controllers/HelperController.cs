﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.SqlClient;
using System.Web.Hosting;
using iTextSharp.text;
using iTextSharp.text.pdf;

using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

using Resources;
using System.Security.Cryptography;
using System.Text;

namespace Fitosanidad.Controllers
{
    public class HelperController : Controller
    {
        /// <summary>
        /// Valida que exista la carpeta para guardar los archivos y que le archivo que se va a guardar no exista y retorna 
        /// la ruta y el nombre del archivo 
        /// </summary>
        /// <param name="_filename">nombre del archivo que se va a guardar</param>
        /// <returns></returns>
        public static string VerifyExportEnvironment(string strFileName)
        {
            try
            {
                if (!System.IO.Directory.Exists(HostingEnvironment.MapPath("\\Downloads")))
                {
                    System.IO.Directory.CreateDirectory(HostingEnvironment.MapPath("\\Downloads"));
                }
                var fileLocation = string.Format("{0}/{1}", HostingEnvironment.MapPath("\\Downloads"), strFileName);

                if (System.IO.File.Exists(fileLocation))
                {
                    System.IO.File.Delete(fileLocation);
                }
                return fileLocation;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Valida que exista la carpeta para guardar los archivos y que le archivo que se va a guardar no exista y retorna 
        /// la ruta y el nombre del archivo 
        /// </summary>
        /// <param name="_filename">nombre del archivo que se va a guardar</param>
        /// <returns></returns>
        public static void VerifyWorkEnvironment()
        {
            try
            {
                if (!System.IO.Directory.Exists(HostingEnvironment.MapPath("\\Uploads")))
                {
                    System.IO.Directory.CreateDirectory(HostingEnvironment.MapPath("\\Uploads"));
                }
                if (!System.IO.Directory.Exists(HostingEnvironment.MapPath("\\Downloads")))
                {
                    System.IO.Directory.CreateDirectory(HostingEnvironment.MapPath("\\Downloads"));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static DataTable GetPivotTable(DataTable table, string columnX, params string[] columnsToIgnore)
        {
            DataTable returnTable = new DataTable();

            if (columnX == "")
                columnX = table.Columns[0].ColumnName;

            returnTable.Columns.Add(columnX);

            List<string> columnXValues = new List<string>();

            List<string> listColumnsToIgnore = new List<string>();
            if (columnsToIgnore.Length > 0)
                listColumnsToIgnore.AddRange(columnsToIgnore);

            if (!listColumnsToIgnore.Contains(columnX))
                listColumnsToIgnore.Add(columnX);

            foreach (DataRow dr in table.Rows)
            {
                string columnXTemp = dr[columnX].ToString();
                if (!columnXValues.Contains(columnXTemp))
                {
                    columnXValues.Add(columnXTemp);
                    returnTable.Columns.Add(columnXTemp);
                }
                else
                {
                    throw new Exception("ERROR: La columna " + columnX + " debe tener valores UNICOS.");
                }
            }

            foreach (DataColumn dc in table.Columns)
            {
                if (!columnXValues.Contains(dc.ColumnName) &&
                    !listColumnsToIgnore.Contains(dc.ColumnName))
                {
                    DataRow dr = returnTable.NewRow();
                    dr[0] = dc.ColumnName;
                    returnTable.Rows.Add(dr);
                }
            }

            for (int i = 0; i < returnTable.Rows.Count; i++)
            {
                for (int j = 1; j < returnTable.Columns.Count; j++)
                {
                    returnTable.Rows[i][j] =
                      table.Rows[j - 1][returnTable.Rows[i][0].ToString()].ToString();
                }
            }

            return returnTable;
        }

        public static Document DataTable2Pdf(DataTable tblDataTable, Document pdfDoc, Font fntContent, int intColTot, int intRecByPage)
        {
            PdfPTable pdfTable = new PdfPTable(intColTot);
            PdfPCell cell = new PdfPCell();


            for (int i = 1; i < tblDataTable.Rows.Count; i++)
            {
                if (i % intRecByPage == 0)
                {
                    pdfDoc.NewPage();
                    for (int j = 0; j < tblDataTable.Columns.Count; j++)
                    {
                        pdfDoc.Add(pdfTable);

                        pdfTable = new PdfPTable(intColTot);

                        cell = new PdfPCell();
                        cell = new PdfPCell(new Phrase(tblDataTable.Rows[i][j].ToString(), fntContent));
                        pdfTable.AddCell(cell);
                    }
                }
                else
                {
                    for (int j = 0; j < tblDataTable.Columns.Count; j++)
                    {
                        cell = new PdfPCell();
                        cell = new PdfPCell(new Phrase(tblDataTable.Rows[i][j].ToString(), fntContent));
                        pdfTable.AddCell(cell);
                    }
                }
            }
            pdfDoc.Add(pdfTable);

            return pdfDoc;
        }

        public static DataTable Csv2DataTable(string _FilePath)
        {
            DataTable dtCsv = new DataTable();
            string Fulltext;

            string FileSaveWithPath = _FilePath;
            using (StreamReader sr = new StreamReader(FileSaveWithPath))
            {
                while (!sr.EndOfStream)
                {
                    Fulltext = sr.ReadToEnd().ToString();
                    string[] rows = Fulltext.Split('\n');
                    for (int i = 0; i < rows.Count() - 1; i++)
                    {
                        Regex CSVParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

                        string[] rowValues = CSVParser.Split(rows[i]);
                        {
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dtCsv.Columns.Add(rowValues[j]);
                                }
                            }
                            else
                            {
                                DataRow dr = dtCsv.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    dr[k] = rowValues[k].ToString();
                                }
                                dtCsv.Rows.Add(dr);
                            }
                        }
                    }
                }
            }
            return dtCsv;
        }

        public static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public static String MD5Text(string text)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                string Pass = GetMd5Hash(md5Hash, text);
                return Pass;
            }

        }
    }
}