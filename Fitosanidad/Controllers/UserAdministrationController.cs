﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class UserAdministrationController : BaseController
    {
        BASEntities basentities = new BASEntities();
        // GET: UserAdministration
        public ActionResult Index()
        {
            basentities = new BASEntities();
            List<Auth_User> Users = basentities.Auth_User.ToList();

            ViewData["Users"] = Users;
            return View();
        }

        public ActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateUserInsert(Auth_User datos)
        {
            
            try
            {
                basentities = new BASEntities();
                var existe = basentities.Auth_User.Where(x => x.Login == datos.Login).FirstOrDefault();
                if(existe != null)
                {
                    var arre = new
                    {
                        Error = 1,
                        Mensaje = "Usuario ya Existe, Verifique!"
                    };
                    return Json(arre, JsonRequestBehavior.AllowGet);
                }
                datos.FlagActive = true;
                basentities.Auth_User.Add(datos);
                basentities.SaveChanges();
                var arr = new
                {
                    Error = 0,
                    Mensaje = "Usuario creado satisfactoriamente!"
                };
                return Json(arr, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult MyInformationForm(Auth_User datos)
        {
            try
            {
                basentities = new BASEntities();
                Auth_User Update = basentities.Auth_User.Where(x => x.UserID == datos.UserID).FirstOrDefault();

                Update.Name = datos.Name;
                Update.LastName = datos.LastName;
                Update.Email = datos.Email;
                Update.DocID = datos.DocID;

                var arr = new
                {
                    Error = 0,
                    Mensaje = "Usuario actualizado correctamente!"
                };

                basentities.SaveChanges();
                return Json(arr, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ChangepaswordForm(Auth_User datos)
        {
            try
            {
                basentities = new BASEntities();
                Auth_User Update = basentities.Auth_User.Where(x => x.UserID == datos.UserID).FirstOrDefault();

                var Pass = HelperController.MD5Text(datos.Password);

                Update.Password = Pass;

                var arr = new
                {
                    Error = 0,
                    Mensaje = "Contraseña actualizada correctamente!"
                };

                basentities.SaveChanges();
                return Json(arr, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}