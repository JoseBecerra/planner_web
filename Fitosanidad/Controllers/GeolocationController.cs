﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Resources;
using Planner.Model;
using Newtonsoft.Json;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class GeolocationController : BaseController
    {
        SIEMBRAS_PLANNEREntities _SIEMBRAS_PLANNEREntities = null;

        public ActionResult Index()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            SelectList ddlOptions = getOptionsByParentId(0);
            ViewData["ddlOptions"] = ddlOptions;

            SelectList ddlDetails = getOptionsByParentId(Convert.ToInt32(ddlOptions.FirstOrDefault().Value));
            ViewData["ddlDetails"] = ddlDetails;

            SelectList lstBusinessUnit = null;
            var aList = _SIEMBRAS_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0 ).OrderBy(x => x.Name).ToList();
            lstBusinessUnit = new SelectList(aList, "ID", "Name".Trim().ToUpper());
            ViewData["lstBusinessUnit"] = lstBusinessUnit;

            getGreenHousebyBusinessUnit(0);

            getCoordinatesPlatesbyBusinessUnit(215);

            return View();
        }

        public ActionResult getMapCoordinates(string intBusinessId, string strQueryFilter)
        {
            getGreenHousebyBusinessUnit(Convert.ToInt32(intBusinessId));

            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var arrCoordinates = _SIEMBRAS_PLANNEREntities.fn_getBusinnesUnitAndGreenhouseCoordinates(Convert.ToInt32(intBusinessId))
                .OrderBy(a => a.GreenhouseId)
                .ThenBy(a => a.CoordinateOrder).ToList();

            var arrFarmCoordinate = new Planner.Model.fn_getBusinnesUnitAndGreenhouseCoordinates_Result();

            if (arrCoordinates.Count > 0)
            {
                arrFarmCoordinate = arrCoordinates.First();
            }

            if (strQueryFilter.Equals("F"))
            {
                arrCoordinates = arrCoordinates.Where(x => x.GreenhouseId == 0).ToList();
            }
            if (strQueryFilter.Equals("I"))
            {
                arrCoordinates = arrCoordinates.Where(x => x.GreenhouseId != 0).ToList();
                if (arrCoordinates.Count > 0)
                {
                    arrCoordinates.Add(arrFarmCoordinate);
                }
            }
            if (strQueryFilter.Equals("T"))
            {
                arrCoordinates = arrCoordinates.ToList();
            }

            var arrResults = arrCoordinates.OrderBy(a => a.GreenhouseId)
                .ThenBy(a => a.CoordinateOrder).ToList();

            return Json(arrResults, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getGreenHousebyBusinessUnit(int intBusinessId)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var greenhouseList = _SIEMBRAS_PLANNEREntities.fn_getGreenHousebyBusinessUnit(Convert.ToInt32(intBusinessId))
                .OrderBy(a => a.Orden)
                .ToList();
            ViewBag.greenhouseList = greenhouseList;

            return PartialView("greenhouseList");
        }

        public ActionResult getCoordinatesPlatesbyBusinessUnit(int intBusinessId)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var CoordinatesPleatesList = _SIEMBRAS_PLANNEREntities.fn_getPhytosanity_CoordinatesPlatesbyBusinessUnit(Convert.ToInt32(intBusinessId))
                .ToList();
            ViewBag.ExternalPlatesList = CoordinatesPleatesList;

            return PartialView("ExternalPlatesList");
        }

        public ActionResult getMapCoordinatesPlatesbyBusinessUnit(int intBusinessId)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var CoordinatesPleatesList = _SIEMBRAS_PLANNEREntities.fn_getPhytosanity_CoordinatesPlatesbyBusinessUnit(Convert.ToInt32(intBusinessId))
                .ToList();

            var arrResults = CoordinatesPleatesList;

            return Json(arrResults, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getGreenhouseArea(string intBusinessUnitId, string intGreenintGreenhouseId)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            int tt = Convert.ToInt32(intGreenintGreenhouseId);

            var arrCoordinates = _SIEMBRAS_PLANNEREntities.fn_getGreenHousebyBusinessUnit(Convert.ToInt32(intBusinessUnitId))
                .Where(x => x.ID == tt)
                .OrderBy(a => a.Orden)
                .ToList();

            return Json(arrCoordinates, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getSensorData(int intBusinessUnitId)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var arrGreenhouseDetails = _SIEMBRAS_PLANNEREntities.fn_getIOT_SensorDataByBusinessUnitId(intBusinessUnitId)
                .GroupBy(x => x.GreenHouseID)
                .ToList();

            return Json(arrGreenhouseDetails, JsonRequestBehavior.AllowGet);
        }

        public SelectList getOptionsByParentId(int ParentId)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var Map_OptionsLst = _SIEMBRAS_PLANNEREntities.Map_Options.Where(x => x.ParentId == ParentId).OrderBy(x => x.Name).ToList();

            SelectList lstMap_Options = new SelectList(Map_OptionsLst, "Id", "Name".Trim().ToUpper());

            return lstMap_Options;
        }

        public ActionResult getOptionsByParentIdJson(int ParentId)
        {
            SelectList lstMap_Options = getOptionsByParentId(ParentId);

            return Json(lstMap_Options, JsonRequestBehavior.AllowGet);
        }
    }
}