﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Planner.Model;
using Resources;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class Planner_SowingController : Controller
    {
        SIEMBRAS_PLANNEREntities _SIEMBRAS_PLANNEREntities = null;

        SelectList businessUnitList = null;
        SelectList productList = null;
        SelectList activityList = null;

        public ActionResult Index()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();


            ViewBag.Planner_SowingList =  _SIEMBRAS_PLANNEREntities.sp_ReportSowing(Convert.ToDateTime("02/01/2019"),Convert.ToDateTime("3/4/2019")).ToList();

            return View(_SIEMBRAS_PLANNEREntities);
        }

        public ActionResult GetData(DateTime startDate, DateTime endDate)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            ViewBag.Planner_SowingList = _SIEMBRAS_PLANNEREntities.sp_ReportSowing(startDate, endDate).ToList();

            return PartialView("Planner_SowingList");
        }

        public ActionResult Visor()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //BusinessUnit
            SelectList lstBusinessUnit = null;
            var aList = _SIEMBRAS_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            lstBusinessUnit = new SelectList(aList, "ID", "Name".Trim().ToUpper());
            ViewData["lstBusinessUnit"] = lstBusinessUnit;

            var a = lstBusinessUnit.First().Value;
            getGreenHousebyBusinessUnit(a);
            
            return View();
        }

        public ActionResult Projection()
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            string currentWeek = BaseController.GetWeek(DateTime.Now);

            //View
            List<SelectListItem> View_Lst = new List<SelectListItem>();
            View_Lst.Add(new SelectListItem() { Text = Resources.Resources.PorDia, Value = "D" });
            View_Lst.Add(new SelectListItem() { Text = Resources.Resources.PorSemana, Value = "S" });
            ViewData["View_Lst"] = new SelectList(View_Lst, "Value", "Text");
            //View

            //Business Unit
            var businessUnitList_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllFlowerGrowing().Where(n => n.ID != 0).OrderBy(x => x.Name).ToList();
            businessUnitList = new SelectList(businessUnitList_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["businessUnitList"] = businessUnitList;
            //Business Unit

            //Product
            var product_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllActiveProducts().OrderBy(x => x.Name).ToList();
            productList = new SelectList(product_Lst, "ID", "Name".Trim().ToUpper());
            ViewData["productList"] = productList;
            //Product

            //Week Range
            SelectList lstWeek = null;
            var aListWeek = _SIEMBRAS_PLANNEREntities.Semanas.Where(n => n.Anio >= 2018).Select(x => new { ID = x.ID, Name = x.Anio + " - " + x.Nombre }).ToList();
            var intSelected = aListWeek.Find(x => x.Name == currentWeek);
            lstWeek = new SelectList(aListWeek, "ID", "Name".Trim().ToUpper(), intSelected.ID);
            ViewBag.lstWeekFrom = lstWeek;
            ViewBag.lstWeekTo = lstWeek;
            //Week Range

            //Activity
            var activity_Lst = _SIEMBRAS_PLANNEREntities.fn_getAllPlanner_ActivityGroup().OrderBy(x => x.Name).ToList();
            activityList = new SelectList(activity_Lst, "ActivityGroupID", "Name".Trim().ToUpper());
            ViewData["activityList"] = activityList;
            //Activity

            return View();
        }
                
        public ActionResult GetProjection(int prmSemanaINI, int prmSemanaFIN, int prmUnidadNegocio, int prmActividad, int prmProductoVegetal, int prmVariedad, int prmSeccion)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            ViewBag.Planner_SowingList = _SIEMBRAS_PLANNEREntities.sp_GetInfoPlannerTask_New(prmSemanaINI, prmSemanaFIN, prmUnidadNegocio, prmActividad, prmProductoVegetal, prmVariedad, prmSeccion);
            return PartialView("ProjectionList");
        }

        public ActionResult getGreenHousebyBusinessUnit(string BusinessUnitID)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            SelectList lstGreenhouse = null;
            var aList = _SIEMBRAS_PLANNEREntities.fn_getGreenHousebyBusinessUnit(Convert.ToInt32(BusinessUnitID)).OrderBy(a => a.Orden).ToList();
            lstGreenhouse = new SelectList(aList, "ID", "Codigo".Trim().ToUpper());
            ViewBag.GreenhouseId = Convert.ToInt32(lstGreenhouse.FirstOrDefault().Value);
            ViewData["lstGreenhouse"] = lstGreenhouse;


            var b = lstGreenhouse.First().Value;
            ViewBag.aListy = _SIEMBRAS_PLANNEREntities.sp_Sowing(Convert.ToInt32(b)).ToList();
            return Json(lstGreenhouse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDataSowing(int greenhouseID)
        {
            _SIEMBRAS_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            ViewBag.aListy =_SIEMBRAS_PLANNEREntities.sp_Sowing(greenhouseID).ToList();

            return PartialView("Visor_List");
        }
    }
}