﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;
using System.Data.SqlClient;
using System.Data;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class AspersionPlanController : BaseController
    {
        public class AspersionPlanConsolidated
        {
            public string AgrochemicalProductsID { get; set; }
            public string Lunes { get; set; }
            public string Martes { get; set; }
            public string Miercoles { get; set; }
            public string Jueves { get; set; }
            public string Luminosity { get; set; }
            public string Viernes { get; set; }
            public string Sabado { get; set; }
            public string Domingo { get; set; }
            public string total { get; set; }
            public int BussinesUnitId { get; set; }
            public int WeekID { get; set; }
        }

        SIEMBRAS_PLANNEREntities siembras_PLANNEREntities = null;
        public ActionResult Index()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var AspersionPlan = siembras_PLANNEREntities.fn_detalleAspersionPlan().ToList();
            ViewBag.datos = AspersionPlan;
            return View();
        }

        public ActionResult AspersionPlanAdd()
        {
            var ActualYear = DateTime.Today.Year;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Datos para dropDown de semanas
            var Week = siembras_PLANNEREntities.Semanas.OrderBy(a => a.ID).Where(x => x.Anio == ActualYear);
            SelectList listWeek = new SelectList((from s in Week
                                                  select new
                                                  {
                                                      ID = s.ID,
                                                      Nombre = s.Anio + "-" + s.Nombre
                                                  }),
                            "Id",
                            "Nombre",
                            null);
            ViewData["listWeek"] = listWeek;

            //Datos para dropDown de compañias
            var company = siembras_PLANNEREntities.Company.Where(x => x.FlagActivo == 1).Distinct().OrderBy(x => x.ID);
            SelectList CompanyList = new SelectList(company, "ID", "Nombre");
            ViewData["CompanyList"] = CompanyList;

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name");
            ViewData["BussinesUnitList"] = BussinesUnitList;

            //Datos para dropDown de compañias
            var PlanProducts = siembras_PLANNEREntities.PlantProducts.Where(x => x.FlagActive == 1).Distinct().OrderBy(x => x.PlantProductName);
            SelectList PlanProductsList = new SelectList(PlanProducts, "PlantProductID", "PlantProductName");
            ViewData["PlanProductsList"] = PlanProductsList;

            //Datos para dropDown de compañias
            var aplyTypes = siembras_PLANNEREntities.Aspersion_ApplyTypes.Where(n => n.ActiveFlag == true).OrderBy(x => x.ApplyTypeID).ToList();
            SelectList aplyTypesList = new SelectList(aplyTypes, "ApplyTypeID", "Name");
            ViewData["aplyTypesList"] = aplyTypesList;

            //Datos para dropDown de formas
            var Forms = siembras_PLANNEREntities.Aspersion_Forms.Where(n => n.ActiveFlag == true).OrderBy(x => x.FormID).ToList();
            SelectList FormsList = new SelectList(Forms, "FormID", "Name");
            ViewData["FormsList"] = FormsList;

            //Datos para dropDown de productos agroquimicos
            var AgrochemicalProduct = siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.Where(n => n.ActiveFlag == true).OrderBy(x => x.Name).ToList();
            SelectList AgrochemicalProductList = new SelectList(AgrochemicalProduct, "AgrochemicalProductsID", "Name");
            ViewData["AgrochemicalProductList"] = AgrochemicalProductList;

            //Datos para dropDown volumen tanque
            var tankVolume = siembras_PLANNEREntities.Aspersion_TankVolume.Where(n => n.ActiveFlag == true).OrderBy(x => x.Name).ToList();
            SelectList tankVolumeList = new SelectList(tankVolume, "TankVolumeID", "Name");
            ViewData["tankVolumeList"] = tankVolumeList;


            return View();
        }

        public ActionResult AspersionPlanAddHead(Aspersion_AspersionPlan datos)
        {

            datos.ActiveFlag = true;
            datos.DateInsert = DateTime.Today;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            Aspersion_AspersionPlan aspersion = siembras_PLANNEREntities.Aspersion_AspersionPlan.Where(x => x.WeekID == datos.WeekID && x.GreenHouseID == datos.GreenHouseID).FirstOrDefault();
            if (aspersion != null)
            {
                return Json("existe", JsonRequestBehavior.AllowGet);
            }

            siembras_PLANNEREntities.Aspersion_AspersionPlan.Add(datos);
            siembras_PLANNEREntities.SaveChanges();
            var AspersionPlanID = datos.AspersionPlanID;

            return Json(AspersionPlanID);
        }

        [HttpPost]
        public void CambiaEstadoHead(Aspersion_AspersionPlan datos)
        {
            try
            {
                siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
                Aspersion_AspersionPlan Update = siembras_PLANNEREntities.Aspersion_AspersionPlan.First(x => x.AspersionPlanID == datos.AspersionPlanID);
                Update.ActiveFlag = datos.ActiveFlag;
                siembras_PLANNEREntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult AspersionPlanListDetail(Aspersion_AspersionPlanDetail datos)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var AspersionPlanList = siembras_PLANNEREntities.fn_DetalleAspersionPlanDetail(datos.AspersionPlanID).Where(x => x.Block > 0).OrderByDescending(a => a.AspersionPlanID).ToList();
            ViewBag.datos = AspersionPlanList;

            return PartialView("AspersionPlanList");
        }

        public JsonResult FiltroBloques()
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var IdCompany = Request.QueryString["IdCompany"];
            if (IdCompany == "NaN")
                IdCompany = "0";
            var Id = Int32.Parse(IdCompany);

            //Datos para dropDown de compañias
            var aList = siembras_PLANNEREntities.fn_getGreenHousebyBusinessUnit(Id).OrderBy(a => a.Orden).ToList();
            SelectList lstGreenhouse = new SelectList(aList, "ID", "Codigo".Trim().ToUpper());
            return Json(lstGreenhouse);
        }

        public ActionResult AspersionPlanAddDetail(Aspersion_AspersionPlanDetail datos, string[] d)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var transac = siembras_PLANNEREntities.Database.BeginTransaction();

            //Consigue el consecutivo del orden de preparación
            var PreparationOrder = siembras_PLANNEREntities.Aspersion_AspersionPlanDetail.Where(
                x => x.AspersionPlanID == datos.AspersionPlanID &&
                x.Block == datos.Block &&
                x.DayNumber == datos.DayNumber &&
                x.AgrochemicalProductsID == datos.AgrochemicalProductsID &&
                x.ProductID == datos.ProductID).Select(x => x.PreparationOrder).DefaultIfEmpty(0).Max();
            var suma = 1;
            for (var i = 0; i < d.Length; i++)
            {
                datos.AgrochemicalProductsID = Convert.ToInt32(d[i]);
                //Verifica que no exista la llave bloque, dia, PPC y producto
                var verificaLlave = siembras_PLANNEREntities.Aspersion_AspersionPlanDetail.Where(
                        x => x.AspersionPlanID == datos.AspersionPlanID &&
                        x.Block == datos.Block &&
                        x.DayNumber == datos.DayNumber &&
                        x.AgrochemicalProductsID == datos.AgrochemicalProductsID &&
                        x.ProductID == datos.ProductID
                    ).FirstOrDefault();

                if (verificaLlave != null)
                {
                    var arr = new
                    {
                        Error = 1,
                        Mensaje = "Error en <b>PPC " + (i+1) + "</b>! Bloque / Día / Producto ya asignado, por favor verifique :" +
                        "<p>• Ya asignado anteriormente</p>" +
                        "<p>• Repetido en este formulario</p>",
                    };
                    transac.Rollback();
                    return Json(arr, JsonRequestBehavior.AllowGet);
                    
                }

                datos.PreparationOrder = (PreparationOrder + suma);

                var date = DateTime.Today;
                datos.DateInsert = date;
                siembras_PLANNEREntities.Aspersion_AspersionPlanDetail.Add(datos);
                siembras_PLANNEREntities.SaveChanges();

                suma++;
            }
            transac.Commit();
            return Json("Ok");
        }

        public ActionResult TraeID(Aspersion_AspersionPlan datos)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            Aspersion_AspersionPlan aspersion = siembras_PLANNEREntities.Aspersion_AspersionPlan.Where(x => x.WeekID == datos.WeekID && x.GreenHouseID == datos.GreenHouseID).FirstOrDefault();

            DateTime Hoy = DateTime.Now;

            var datosDetalle = 0;
            if (aspersion != null)
            {
                datosDetalle = siembras_PLANNEREntities.Aspersion_AspersionPlanDetail.Where(x => x.AspersionPlanID == aspersion.AspersionPlanID).Count();
            }
                
            if (aspersion == null)
            {
                var arr = new
                {
                    Id = 0,
                    Ctrl = 1
                };
                return Json(arr, JsonRequestBehavior.AllowGet);
            }
            else if(aspersion != null && datosDetalle == 0)
            {
                var arr = new
                {
                    Id = aspersion.AspersionPlanID,
                    Ctrl = 3
                };

                return Json(arr, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var arr = new
                {
                    Id = aspersion.AspersionPlanID,
                    Ctrl = 3
                };
                return Json(arr, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AspersionPlanEdit(int Id)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var datosCabecera = siembras_PLANNEREntities.Aspersion_AspersionPlan.Where(x => x.AspersionPlanID == Id).FirstOrDefault();
            Aspersion_AspersionPlan datosCabeceraSelect = siembras_PLANNEREntities.Aspersion_AspersionPlan.First(x => x.AspersionPlanID == Id);
            ViewBag.AspersionPlanID = datosCabecera.AspersionPlanID;
            var ActualYear = DateTime.Today.Year;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Datos para dropDown de semanas
            var Week = siembras_PLANNEREntities.Semanas.OrderBy(a => a.ID).Where(x => x.Anio == ActualYear);
            SelectList listWeek = new SelectList((from s in Week
                                                  select new
                                                  {
                                                      Id = s.ID,
                                                      Nombre = s.Anio + "-" + s.Nombre
                                                  }),
                            "Id",
                            "Nombre",
                            datosCabeceraSelect.WeekID);
            ViewData["listWeek"] = listWeek;

            //Datos para dropDown de compañias
            var company = siembras_PLANNEREntities.Company.Where(x => x.FlagActivo == 1).Distinct().OrderBy(x => x.ID);
            SelectList CompanyList = new SelectList(company, "Id", "Nombre");
            ViewData["CompanyList"] = CompanyList;

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name", datosCabeceraSelect.GreenHouseID);
            ViewData["BussinesUnitList"] = BussinesUnitList;

            //Datos para dropDown de compañias
            var aplyTypes = siembras_PLANNEREntities.Aspersion_ApplyTypes.Where(n => n.ActiveFlag == true).OrderBy(x => x.ApplyTypeID).ToList();
            SelectList aplyTypesList = new SelectList(aplyTypes, "ApplyTypeID", "Name");
            ViewData["aplyTypesList"] = aplyTypesList;

            //Datos para dropDown de formas
            var Forms = siembras_PLANNEREntities.Aspersion_Forms.Where(n => n.ActiveFlag == true).OrderBy(x => x.FormID).ToList();
            SelectList FormsList = new SelectList(Forms, "Name", "Name");
            ViewData["FormsList"] = FormsList;

            //Datos para dropDown de productos agroquimicos
            var AgrochemicalProduct = siembras_PLANNEREntities.Aspersion_AgrochemicalProducts.Where(n => n.ActiveFlag == true).OrderBy(x => x.Name).ToList();
            SelectList AgrochemicalProductList = new SelectList(AgrochemicalProduct, "AgrochemicalProductsID", "Name");
            ViewData["AgrochemicalProductList"] = AgrochemicalProductList;

            //Datos para dropDown volumen tanque
            var tankVolume = siembras_PLANNEREntities.Aspersion_TankVolume.Where(n => n.ActiveFlag == true).OrderBy(x => x.Name).ToList();
            SelectList tankVolumeList = new SelectList(tankVolume, "Name", "Name");
            ViewData["tankVolumeList"] = tankVolumeList;

            //Datos para DropDown de  lanza
            var ReferenceSpear = siembras_PLANNEREntities.Aspersion_ReferenceSpear.Where(x => x.ActiveFlag == true).ToList();
            SelectList ReferenceSpearList = new SelectList(ReferenceSpear, "ReferenceSpearID", "Name");
            ViewData["ReferenceSpearList"] = ReferenceSpearList;

            //Datos para supervisor
            var AspersionEmployes = siembras_PLANNEREntities.Aspersion_AspersionEmployees.Where(x => x.ActiveFlag == true && x.RolID == 1);
            SelectList AspersionEmployesList = new SelectList((from s in AspersionEmployes
                                                               select new
                                                               {
                                                                   Id = s.AspersionEmployeesID,
                                                                   Nombre = s.Name + " " + s.LastName
                                                               }),
                            "Id",
                            "Nombre", 
                            null);
            ViewData["AspersionEmployesList"] = AspersionEmployesList;

            return View();
        }

        public ActionResult AspersioPlanOutputTable(int Id)
        {
            var ActualYear = DateTime.Today.Year;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Datos para dropDown de semanas
            var Week = siembras_PLANNEREntities.Semanas.OrderBy(a => a.ID).Where(x => x.Anio == ActualYear);
            SelectList listWeek = new SelectList((from s in Week
                                                  select new
                                                  {
                                                      Id = s.ID,
                                                      Nombre = s.Anio + "-" + s.Nombre
                                                  }),
                            "Id",
                            "Nombre",
                            null);
            ViewData["listWeek"] = listWeek;

            //Datos para supervisor
            var AspersionEmployes = siembras_PLANNEREntities.Aspersion_AspersionEmployees.Where(x => x.ActiveFlag == true && x.RolID == 1);
            SelectList AspersionEmployesList = new SelectList((from s in AspersionEmployes
                                                               select new
                                                               {
                                                                   Id = s.AspersionEmployeesID,
                                                                   Nombre = s.Name + " " + s.LastName
                                                               }),
                            "Id",
                            "Nombre",
                            null);
            ViewData["AspersionEmployesList"] = AspersionEmployesList;

            var AspersionPlan = siembras_PLANNEREntities.fn_detalleAspersionPlan().Where(x => x.AspersionPlanID == Id);
            ViewBag.AspersionPlan = AspersionPlan;
            ViewBag.Id = Id;


            var PlanProducts = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id).ToList();
            ViewBag.PlanProducts = PlanProducts;
            return View();
        }

        public ActionResult cargaTabla(int Id)
        { 
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            var PlanProducts = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id).ToList();
            //var PlanProducts = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(datos.AspersionPlanID, datos.ProductID, datos.WeekID, datos.GreenHouseID, datos.Anio).ToList();
            SelectList PlanProductsList = new SelectList(PlanProducts, "PlantProductID", "PlantProductName");
            return Json(PlanProducts, JsonRequestBehavior.AllowGet);
        }

        public ActionResult imprimeEtiquetas(int Id)
        {
            var IdDetail = Request.QueryString["IdDetail"];
            var WeekID = Request.QueryString["WeekID"];
            var NumberDay = Request.QueryString["NumberDay"];
            var AspersionEmployeesID = Request.QueryString["AspersionEmployeesID"];


            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            //List<fn_AspersioPlanOutputTable_Result> PlanProducts = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id);
            var PlanProducts = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id);
            if (Convert.ToInt32(IdDetail) > 0)
            {
                int IDDetail = Convert.ToInt32(IdDetail);
                PlanProducts = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id).Where(x => x.AspersionPlanDetailID == IDDetail);
            }
            else
            {
                //Filtros del formulario
                if (NumberDay != "")
                {
                    int ND = Convert.ToInt32(NumberDay);
                    PlanProducts = PlanProducts.Where(x => x.DiaID == ND);
                }

                if (WeekID != "")
                {
                    int WI = Convert.ToInt32(WeekID);
                    PlanProducts = PlanProducts.Where(x => x.SemanaId == WI);
                }

                if (AspersionEmployeesID != "")
                {
                    int AE = Convert.ToInt32(AspersionEmployeesID);
                    PlanProducts = PlanProducts.Where(x => x.AspersionEmployeesID == AE);
                }
            }
            List<fn_AspersioPlanOutputTable_Result> PlanProductsList = PlanProducts.ToList();
            return IMPZebra(PlanProductsList);
        }

        public ActionResult IMPDatamax(List<fn_AspersioPlanOutputTable_Result> PlanProducts)
        {
            var mes = DateTime.Now.Month;
            var day = DateTime.Now.Day;
            var hour = DateTime.Now.Hour;
            var second = DateTime.Now.Second;
            var NombreArchivo = "LPFASP" + "00" + mes + "00" + day + "00" + hour + "00" + second + ".txt";

            string strPath = HelperController.VerifyExportEnvironment(NombreArchivo);
            StreamWriter sw = new StreamWriter(strPath);

            var cont = 1;
            var contadorGeneral = 0;
            var contadorTotal = PlanProducts.Count;
            int cierre = 0;
            foreach (var x in PlanProducts)
            {
                var tarros = Math.Ceiling(((x.BedNumber * x.LiterPerBed) / x.TankVolume));

                if (x.Sald % x.TankVolume == 0)
                {
                    x.Sald = 0;
                }
                else
                {
                    x.Sald = Math.Round((decimal)(x.Sald), 2);
                    //Se hace de esta forma ya que el Abs() no funciona
                    if (x.Sald < 0)
                    {
                        x.Sald = x.Sald * (-1);
                    }
                }
                for (var i = 0; i < tarros; i++)
                {
                    //Etiqueta izquierda
                    if (contadorGeneral % 2 == 0)
                    {
                        sw.WriteLine("L");
                        sw.WriteLine("D11");
                        sw.WriteLine("H16");
                        var sem = x.WeekID.Length;
                        sw.WriteLine("131100001200010" + x.WeekID.Substring((sem - 2), 2));
                        sw.WriteLine("131100001200040-" + x.DayNumber.Substring(0, 3));
                        sw.WriteLine("131100001200080-" + cont);
                        sw.WriteLine("131100001200110BL: " + x.Block);
                        sw.WriteLine("131100001000010" + x.GreenHouseID);
                        sw.WriteLine("131100000800010CAT. TOX:" + x.ToxicologicalCategoryID);
                        sw.WriteLine("131100000600010" + x.ProductID);
                        sw.WriteLine("131100000400010" + x.AgrochemicalProductsID);
                        decimal cantidad = 0;
                        decimal saldo = 0;
                        decimal cantidadTotal = (decimal)(x.dose * x.Density) * x.BedNumber * x.LiterPerBed;
                        if (x.MeasurementUnitsID == "CC")
                        {
                            cantidad = (decimal)(x.TankVolume * x.dose * x.Density);
                            saldo = (decimal)(x.Sald * x.dose * x.Density);
                        }
                        else
                        {
                            cantidad = (decimal)(x.TankVolume * x.dose);
                            saldo = (decimal)(x.Sald * x.dose);
                        }

                        //Valida que tiene saldo
                        if (i == (tarros - 1) && x.Sald > 0)
                        {
                            sw.WriteLine("131100000200010" + Math.Ceiling(saldo) + " " + x.MeasurementUnitsID);
                        }
                        else if (cantidadTotal < cantidad)
                        {
                            sw.WriteLine("131100000200010" + Math.Ceiling(cantidadTotal) + " " + x.MeasurementUnitsID);
                        }
                        else
                        {
                            sw.WriteLine("131100000200010" + Math.Ceiling(cantidad) + " " + x.MeasurementUnitsID);
                        }
                        cierre = 0;
                    }
                    //Etiqueta derecha
                    if (contadorGeneral % 2 != 0)
                    {
                        var sem = x.WeekID.Length;
                        sw.WriteLine("131100001200180" + x.WeekID.Substring((sem - 2), 2));
                        sw.WriteLine("131100001200210-" + x.DayNumber.Substring(0, 3));
                        sw.WriteLine("131100001200250-" + cont);
                        sw.WriteLine("131100001200250-" + cont);
                        sw.WriteLine("131100001200280BL: " + x.Block);
                        sw.WriteLine("131100001000180" + x.GreenHouseID);
                        sw.WriteLine("131100000800180CAT. TOX:" + x.ToxicologicalCategoryID);
                        sw.WriteLine("131100000600180" + x.ProductID);
                        sw.WriteLine("131100000400180" + x.AgrochemicalProductsID);
                        decimal cantidad = 0;
                        decimal saldo = 0;
                        decimal cantidadTotal = (decimal)(x.dose * x.Density) * x.BedNumber * x.LiterPerBed;
                        if (x.MeasurementUnitsID == "CC")
                        {
                            cantidad = (decimal)(x.TankVolume * x.dose * x.Density);
                            saldo = (decimal)(x.Sald * x.dose * x.Density);
                        }
                        else
                        {
                            cantidad = (decimal)(x.TankVolume * x.dose);
                            saldo = (decimal)(x.Sald * x.dose);
                        }

                        if (i == (tarros - 1) && x.Sald > 0)
                        {
                            sw.WriteLine("131100000200180" + Math.Ceiling(saldo) + " " + x.MeasurementUnitsID);
                        }
                        else if (cantidadTotal < cantidad)
                        {
                            sw.WriteLine("131100000200180" + Math.Ceiling(cantidadTotal) + " " + x.MeasurementUnitsID);
                        }
                        else
                        {
                            sw.WriteLine("131100000200180" + Math.Ceiling(cantidad) + " " + x.MeasurementUnitsID);
                        }
                        sw.WriteLine("Q1");
                        sw.WriteLine("E");
                        cierre = 1;
                    }
                    contadorGeneral++;
                    cont++;
                }
            }
            if (cierre == 0)
            {
                sw.WriteLine("Q1");
                sw.WriteLine("E");
            }
            sw.Close();

            byte[] fileBytes = System.IO.File.ReadAllBytes(strPath);
            string fileName = NombreArchivo;
            System.IO.File.Delete(strPath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult IMPZebra(List<fn_AspersioPlanOutputTable_Result> PlanProducts)
        {
            var mes = DateTime.Now.Month;
            var day = DateTime.Now.Day;
            var hour = DateTime.Now.Hour;
            var second = DateTime.Now.Second;
            var NombreArchivo = "LPFASP" + "00" + mes + "00" + day + "00" + hour + "00" + second + ".txt";

            string strPath = HelperController.VerifyExportEnvironment(NombreArchivo);
            StreamWriter sw = new StreamWriter(strPath);

            var cont = 1;
            var contadorGeneral = 0;
            var contadorTotal = PlanProducts.Count;
            int cierre = 0;
            foreach (var x in PlanProducts)
            {
                var tarros = Math.Ceiling(((x.BedNumber * x.LiterPerBed) / x.TankVolume));

                if (x.Sald % x.TankVolume == 0)
                {
                    x.Sald = 0;
                }
                else
                {
                    x.Sald = Math.Round((decimal)(x.Sald), 2);
                    //Se hace de esta forma ya que el Abs() no funciona
                    if (x.Sald < 0)
                    {
                        x.Sald = x.Sald * (-1);
                    }
                }

                //Valida que la cantidad menor al litaje es menor sea saldo

                for (var i = 0; i < tarros; i++)
                {
                    //Etiqueta izquierda
                    if (contadorGeneral % 2 == 0)
                    {
                        sw.WriteLine("^XA");
                        sw.WriteLine("^PW800");
                        sw.WriteLine("^LH0,0");
                        var sem = x.WeekID.Length;
                        sw.WriteLine("^FO40,20^A0N10,50^FD" + x.WeekID.Substring((sem - 2), 2) + "^FS");
                        sw.WriteLine("^FO80,30^A0N10,30^FD - " + x.DayNumber.Substring(0, 3) + "^FS");
                        sw.WriteLine("^FO145,30^A0N10,30^FD -" + cont + "^FS");
                        sw.WriteLine("^FO220,30^A0N10,30^FD BL: " + x.Block + "^FS");
                        sw.WriteLine("^FO40,70^A0N10,40^FD" + x.GreenHouseID + "^FS");
                        sw.WriteLine("^FO40,120^A0N10,40^FDCAT. TOX:" + x.ToxicologicalCategoryID + "^FS");
                        sw.WriteLine("^FO40,170^A0N10,40^FD" + x.ProductID + "^FS");
                        sw.WriteLine("^FO40,220^A0N10,40^FD" + x.AgrochemicalProductsID + "^FS");
                        decimal cantidad = 0;
                        decimal saldo = 0;
                        decimal cantidadTotal = (decimal)(x.dose * x.Density) * x.BedNumber * x.LiterPerBed;
                        if (x.MeasurementUnitsID == "CC")
                        {
                            cantidad = (decimal)(x.TankVolume * x.dose * x.Density);
                            saldo = (decimal)(x.Sald * x.dose * x.Density);
                        }
                        else
                        {
                            cantidad = (decimal)(x.TankVolume * x.dose);
                            saldo = (decimal)(x.Sald * x.dose);
                        }

                        //Valida que tiene saldo
                        if ((i == (tarros - 1) && x.Sald > 0))
                        {
                            sw.WriteLine("^FO40,260^A0N10,50^FD" + Math.Ceiling(saldo) + " " + x.MeasurementUnitsID + "^FS");
                        }
                        else if(cantidadTotal < cantidad)
                        {
                            sw.WriteLine("^FO40,260^A0N10,50^FD" + Math.Ceiling(cantidadTotal) + " " + x.MeasurementUnitsID + "^FS");
                        }
                        else
                        {
                            sw.WriteLine("^FO40,260^A0N10,50^FD" + Math.Ceiling(cantidad) + " " + x.MeasurementUnitsID + "^FS");
                        }
                        cierre = 0;
                    }
                    //Etiqueta derecha
                    if (contadorGeneral % 2 != 0)
                    {
                        var sem = x.WeekID.Length;
                        sw.WriteLine("^FO460,20^A0N10,50^FD" + x.WeekID.Substring((sem - 2), 2) + "^FS");
                        sw.WriteLine("^FO500,30^A0N10,30^FD - " + x.DayNumber.Substring(0, 3) + "^FS");
                        sw.WriteLine("^FO565,30^A0N10,30^FD -" + cont + "^FS");
                        sw.WriteLine("^FO680,30^A0N10,30^FD BL: " + x.Block + "^FS");
                        sw.WriteLine("^FO460,70^A0N10,40^FD" + x.GreenHouseID + "^FS");
                        sw.WriteLine("^FO460,120^A0N10,40^FDCAT. TOX:" + x.ToxicologicalCategoryID + "^FS");
                        sw.WriteLine("^FO460,170^A0N10,40^FD" + x.ProductID + "^FS");
                        sw.WriteLine("^FO460,220^A0N10,40^FD" + x.AgrochemicalProductsID + "^FS");
                        decimal cantidad = 0;
                        decimal saldo = 0;
                        decimal cantidadTotal = (decimal)(x.dose * x.Density) * x.BedNumber * x.LiterPerBed;
                        if (x.MeasurementUnitsID == "CC")
                        {
                            cantidad = (decimal)(x.TankVolume * x.dose * x.Density);
                            saldo = (decimal)(x.Sald * x.dose * x.Density);
                        }
                        else
                        {
                            cantidad = (decimal)(x.TankVolume * x.dose);
                            saldo = (decimal)(x.Sald * x.dose);
                        }

                        if (i == (tarros - 1) && x.Sald > 0)
                        {
                            sw.WriteLine("^FO460,260^A0N10,50^FD" + Math.Ceiling(saldo) + " " + x.MeasurementUnitsID + "^FS");
                        }
                        else if (cantidadTotal < cantidad)
                        {
                            sw.WriteLine("^FO460,260^A0N10,50^FD" + Math.Ceiling(cantidadTotal) + " " + x.MeasurementUnitsID + "^FS");
                        }
                        else
                        {
                            sw.WriteLine("^FO460,260^A0N10,50^FD" + Math.Ceiling(cantidad) + " " + x.MeasurementUnitsID + "^FS");
                        }
                        sw.WriteLine("^PQ1");
                        sw.WriteLine("^XZ");
                        cierre = 1;
                    }
                    contadorGeneral++;
                    cont++;
                }
            }
            if (cierre == 0)
            {
                sw.WriteLine("^PQ1");
                sw.WriteLine("^XZ");
            }
            sw.Close();

            byte[] fileBytes = System.IO.File.ReadAllBytes(strPath);
            string fileName = NombreArchivo;
            System.IO.File.Delete(strPath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        public ActionResult OutputTableFilter(int? WeekID, int? NumberDay, int? AspersionEmployeesID, int? Id)
        {
            var ActualYear = DateTime.Today.Year;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Datos para dropDown de semanas
            var Week = siembras_PLANNEREntities.Semanas.OrderBy(a => a.ID).Where(x => x.Anio == ActualYear);
            SelectList listWeek = new SelectList((from s in Week
                                                  select new
                                                  {
                                                      Id = s.ID,
                                                      Nombre = s.Anio + "-" + s.Nombre
                                                  }),
                            "Id",
                            "Nombre",
                            null);
            ViewData["listWeek"] = listWeek;

            //Datos para supervisor
            var AspersionEmployes = siembras_PLANNEREntities.Aspersion_AspersionEmployees.Where(x => x.ActiveFlag == true && x.RolID == 1);
            SelectList AspersionEmployesList = new SelectList((from s in AspersionEmployes
                                                               select new
                                                               {
                                                                   Id = s.AspersionEmployeesID,
                                                                   Nombre = s.Name + " " + s.LastName
                                                               }),
                            "Id",
                            "Nombre",
                            null);
            ViewData["AspersionEmployesList"] = AspersionEmployesList;

            var AspersionPlan = siembras_PLANNEREntities.fn_detalleAspersionPlan().Where(x => x.AspersionPlanID == Id);
            ViewBag.AspersionPlan = AspersionPlan;
            ViewBag.Id = Id;


            var PlanProducts = siembras_PLANNEREntities.fn_AspersioPlanOutputTable(Id);

            //Filtros del formulario
            if(NumberDay != null)
            {
                PlanProducts = PlanProducts.Where(x => x.DiaID == NumberDay);
            }

            if(WeekID != null)
            {
                PlanProducts = PlanProducts.Where(x => x.SemanaId == WeekID);
            }

            if(AspersionEmployeesID != null)
            {
                PlanProducts = PlanProducts.Where(x => x.AspersionEmployeesID == AspersionEmployeesID);
            }

            ViewBag.PlanProducts = PlanProducts;

            return View("AspersioPlanOutputTable");
        }

        public ActionResult ProductByBlock(int BlockId)
        {
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();
            var ProductByBlock = siembras_PLANNEREntities.fn_getProductByBlock(BlockId).ToList();
            SelectList ProductByBlockList = new SelectList(ProductByBlock, "PlantProductID", "PlantProductName");
            return Json(ProductByBlockList);
        }

        public ActionResult Consolidated()
        {
            var ActualYear = DateTime.Today.Year;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            //Datos para dropDown de semanas
            var Week = siembras_PLANNEREntities.Semanas.OrderBy(a => a.ID).Where(x => x.Anio == ActualYear);
            SelectList listWeek = new SelectList((from s in Week
                                                  select new
                                                  {
                                                      Id = s.ID,
                                                      Nombre = s.Anio + "-" + s.Nombre
                                                  }),
                            "Id",
                            "Nombre",
                            null);
            ViewData["listWeek"] = listWeek;

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name", null);
            ViewData["BussinesUnitList"] = BussinesUnitList;

            SqlConnection entityConnection = (SqlConnection)siembras_PLANNEREntities.Database.Connection;
            SqlConnection cnn = entityConnection;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_AspersionPlanConsolidated";

            SqlParameter DiaID = new SqlParameter("@DiaID", SqlDbType.Int);
            SqlParameter BusinessUnitID = new SqlParameter("@BusinessUnitID", SqlDbType.Int);

            DiaID.Value = 0;
            BusinessUnitID.Value = 0;

            cmd.Parameters.Add(DiaID);
            cmd.Parameters.Add(BusinessUnitID);

            cnn.Open();

            List<AspersionPlanConsolidated> AspersionPlanConsolidatedList = new List<AspersionPlanConsolidated>();

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    AspersionPlanConsolidated _AspersionPlanConsolidated = new AspersionPlanConsolidated();

                    _AspersionPlanConsolidated.AgrochemicalProductsID = dr[0].ToString();
                    _AspersionPlanConsolidated.Lunes = dr[1].ToString();
                    _AspersionPlanConsolidated.Martes = dr[2].ToString();
                    _AspersionPlanConsolidated.Miercoles = dr[3].ToString();
                    _AspersionPlanConsolidated.Jueves = dr[4].ToString();
                    _AspersionPlanConsolidated.Viernes = dr[5].ToString();
                    _AspersionPlanConsolidated.Sabado = dr[6].ToString();
                    _AspersionPlanConsolidated.Domingo = dr[7].ToString();
                    _AspersionPlanConsolidated.total = dr[8].ToString();

                    AspersionPlanConsolidatedList.Add(_AspersionPlanConsolidated);
                }

                dr.Close();
            }

            cnn.Close();

            ViewBag.Consolidate = AspersionPlanConsolidatedList;

            return View();
        }

        [HttpPost]
        public ActionResult ConsolidatedFiltred(int? BussinesUnitId, int? WeekID)
        {
            if(BussinesUnitId == null)
            {
                BussinesUnitId = 0;
            }

            if (WeekID == null)
            {
                WeekID = 0;
            }

            var ActualYear = DateTime.Today.Year;
            siembras_PLANNEREntities = new SIEMBRAS_PLANNEREntities();

            AspersionPlanConsolidated DatDropDown = new AspersionPlanConsolidated();
            DatDropDown.WeekID = Convert.ToInt32(WeekID);
            DatDropDown.BussinesUnitId = Convert.ToInt32(BussinesUnitId);

            //Datos para dropDown de semanas
            var Week = siembras_PLANNEREntities.Semanas.OrderBy(a => a.ID).Where(x => x.Anio == ActualYear);
            SelectList listWeek = new SelectList((from s in Week
                                                  select new
                                                  {
                                                      Id = s.ID,
                                                      Nombre = s.Anio + "-" + s.Nombre
                                                  }),
                            "Id",
                            "Nombre",
                            DatDropDown.WeekID);
            ViewData["listWeek"] = listWeek;

            //Datos para dropDown de compañias
            var BussinesUnit = siembras_PLANNEREntities.fn_getAllFlowerGrowing().ToList();
            SelectList BussinesUnitList = new SelectList(BussinesUnit, "ID", "Name", DatDropDown.BussinesUnitId);
            ViewData["BussinesUnitList"] = BussinesUnitList;

            SqlConnection entityConnection = (SqlConnection)siembras_PLANNEREntities.Database.Connection;
            SqlConnection cnn = entityConnection;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_AspersionPlanConsolidated";

            SqlParameter DiaID = new SqlParameter("@DiaID", SqlDbType.Int);
            SqlParameter BusinessUnitID = new SqlParameter("@BusinessUnitID", SqlDbType.Int);

            DiaID.Value = WeekID;
            BusinessUnitID.Value = BussinesUnitId;

            cmd.Parameters.Add(DiaID);
            cmd.Parameters.Add(BusinessUnitID);

            cnn.Open();

            List<AspersionPlanConsolidated> AspersionPlanConsolidatedList = new List<AspersionPlanConsolidated>();

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    AspersionPlanConsolidated _AspersionPlanConsolidated = new AspersionPlanConsolidated();

                    _AspersionPlanConsolidated.AgrochemicalProductsID = dr[0].ToString();
                    _AspersionPlanConsolidated.Lunes = dr[1].ToString();
                    _AspersionPlanConsolidated.Martes = dr[2].ToString();
                    _AspersionPlanConsolidated.Miercoles = dr[3].ToString();
                    _AspersionPlanConsolidated.Jueves = dr[4].ToString();
                    _AspersionPlanConsolidated.Viernes = dr[5].ToString();
                    _AspersionPlanConsolidated.Sabado = dr[6].ToString();
                    _AspersionPlanConsolidated.Domingo = dr[7].ToString();
                    _AspersionPlanConsolidated.total = dr[8].ToString();

                    AspersionPlanConsolidatedList.Add(_AspersionPlanConsolidated);
                }

                dr.Close();
            }

            cnn.Close();

            ViewBag.Consolidate = AspersionPlanConsolidatedList;

            return Json(AspersionPlanConsolidatedList);
        }
    }
}