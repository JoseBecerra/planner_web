﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planner.Model;

namespace Fitosanidad.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        BASEntities basentities = new BASEntities();
        public ActionResult Index()
        {
            basentities = new BASEntities();
            if(Session["UserID"] == null)
            {
                return View("../Account/Login");
            }
            else
            {
                int Id = Convert.ToInt32(Session["UserID"]);
                var Users = basentities.Auth_User.Where(x => x.UserID == Id).FirstOrDefault();

                ViewBag.Users = Users;
                return View(Users);
            }
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}