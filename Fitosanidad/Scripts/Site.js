﻿$(document).ready(function () {
    var bootstrapTooltip = $.fn.tooltip.noConflict();
    $.fn.bstooltip = bootstrapTooltip;
    $(this).find("button").each(function () {
        $(this).bstooltip();
    })
});

jQuery(document).ready(function () {
    jQuery("#divNotifications").hide();
    jQuery('#sidebar').toggleClass('active');

    CargarMenuLateral();

    jQuery("#btnNotifications").click(function (e) {
        jQuery("#divNotifications").hide();
    });

    $.getBadge = function (hadler) {
        $.getJSON('/Json/GetBadge', function (data) {
            if (data == "undefined" || data == null) {
                Notifications("information", GetResource("NoSeEncontraronDatos"))
                return;
            }
            hadler(data);
        });
    };

    $.getCountry = function (hadler) {
        $.getJSON('/Json/GetCountry', function (data) {
            if (data == "undefined" || data == null) {
                Notifications("information", GetResource("NoSeEncontraronDatos"))
                return;
            }
            hadler(data);
        });
    }
});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function Notifications(_type, _message) {
    if (_message == undefined) {
        return;
    }
    jQuery("#divNotifications").hide();

    jQuery("#strMessage").empty();
    jQuery("#strMessage").append(_message);

    if (_type == "alert") {
        jQuery("#divNotifications").attr("class", "alert alert-info alert-dismissable");
    }
    if (_type == "success") {
        jQuery("#divNotifications").attr("class", "alert alert-success alert-dismissable");
    }
    if (_type == "error") {
        jQuery("#divNotifications").attr("class", "alert alert-danger alert-dismissable");
    }
    if (_type == "warning") {
        jQuery("#divNotifications").attr("class", "alert alert-warning alert-dismissable");
    }
    if (_type == "information") {
        jQuery("#divNotifications").attr("class", "alert alert-danger alert-dismissable");
    }
    jQuery("#divNotifications").show();
    setTimeout(function () {
        jQuery("#divNotifications").hide();
    }, 5000);
};

function GetResource(key, reemplazo) {
    var _key = "";
    var laArray = new Array();

    if (reemplazo !== null) {
        laArray = reemplazo;
    }

    var data = {
        "_Key": key,
        "_Parametros": laArray,
    }
    $.ajax({
        type: "POST",
        url: "/Base/GetResource",
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        success: function (d) {
            _key = d;
        },
        error: function (xhr, textStatus, errorThrown) {
            _key = errorThrown;
        },
        dataType: "Json",
        async: false
    });
    return _key
}

/******************
* Procedimiento que bloquea la pantalla mientras
* se hace un proceso
******************/
function BlockScreen() {
    jQuery.blockUI();
    jQuery.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#002744',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }, message: '<img src="/Image/ajax-loader.gif" />'
    });
};

/******************
*   Procedimiento que desbloquea la pantalla
*   después que termina un proceso
******************/
function UnlockScreen() {
    setTimeout(jQuery.unblockUI, 1000);
};

function soloNumeros(e) {
    var charCode = (e.which) ? e.which : e.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function soloNumerosDecimal(evt, input) {
    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
    var key = window.Event ? evt.which : evt.keyCode;
    var chark = String.fromCharCode(key);
    var tempValue = input.value + chark;
    if (key >= 48 && key <= 57) {
        if (filter(tempValue) === false) {
            return false;
        } else {
            return true;
        }
    } else {
        if (key == 8 || key == 13 || key == 0) {
            return true;
        } else if (key == 46) {
            if (filter(tempValue) === false) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
function filter(__val__) {
    var preg = /^([0-9]+\.?[0-9]{0,2})$/;
    if (preg.test(__val__) === true) {
        return true;
    } else {
        return false;
    }

}

function getFormattedDate(date) {
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return day + '/' + month + '/' + year;
}

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

function NomMes(Mes) {
    var _NomMes = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

    return _NomMes[Mes];
}

function Meses() {
    var moment = require('moment');

    var count = 0;
    var months = [];
    while (count < 12) months.push(moment().month(count++).format("MMMM"));
}

function EMailValid(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function CargarMenuLateral(e) {
    return;

    if (e) {
        e.preventDefault();
    }
    BloquearPantalla();

    var _key = "";
    var data = null;
    $.ajax({
        type: "POST",
        url: "/Helper/CargarOpciones",
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery("#tree").igTree({
                singleBranchExpand: true,
                dataSource: data,
                dataSourceType: "json",
                initialExpandDepth: -1,
                pathSeparator: ".",
                bindings: {
                    textKey: "Text",
                    valueKey: "Value",
                    childDataProperty: "Submodulos"
                },
                nodeClick: function (evt, ui) {
                    OpcionesDetail(ui.node.data.Value);
                },
                dragAndDrop: false,
            });
        },
        error: function (xhr, textStatus, errorThrown) {
            Notifications("error", GetResource("Sepresentaronerroresalintentargenerarelproceso") + " ERROR: " + errorThrown)
        },
        dataType: "Json",
        async: false
    });

    DesBloquearPantalla();
}

$(document).ajaxStart(function () {
    //BlockScreen();
});
$(document).ajaxStop(function () {
    //UnlockScreen();
});

function ReplaceNumberWithCommas(intNumberToConvert) {
    var components = intNumberToConvert.toString().split(".");
    components[0] = components[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return components.join(".");
}

function ValidateDate(datStartDate, datEndDate) {
    if (datStartDate != null && datEndDate != null) {
        if (datStartDate > datEndDate) {
            Notifications("alert", "The start date should not exceed the end date");
            return false;
        }
    }
    return true;
}
