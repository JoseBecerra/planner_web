﻿jQuery(document).ready(function () {
    jQuery("#dataSensor").click(function () {
        jQuery('<table>')
            .append(
            jQuery("#tableCoordinatesList").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Coordinates",
                filename: "Coordinates.xls"
            });
    });
    jQuery("#tableCoordinatesList").dataTable();
});