﻿jQuery(document).ready(function () {
    jQuery("#datagreenhouseList").click(function () {
        jQuery('<table>')
            .append(
                jQuery("#tablegreenhouseList").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "GreenhousebyBusinessUnit" //do not include extension
            });
    });
});