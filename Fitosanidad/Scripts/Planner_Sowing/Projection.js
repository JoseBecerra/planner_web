﻿jQuery(document).ready(function () {

    jQuery("#dataPlanner_Sowing").click(function () {
        jQuery('<table>')
            .append(
                jQuery("#tablePlanner_Sowing").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "Planner_Sowing_Data" //do not include extension
            });
    });
    jQuery("#tablePlanner_Sowing").dataTable();
});

function Search() {
    var prmSemanaINI = parseInt($("#ddlWeekFrom").find(":selected").val());
    var prmSemanaFIN = parseInt($("#ddlWeekTo").find(":selected").val());
    var prmUnidadNegocio = parseInt($("#ddlBusinessUnit").find(":selected").val());
    var prmActividad = parseInt($("#ddlActivity").find(":selected").val());
    var prmProductoVegetal = parseInt($("#ddlProduct").find(":selected").val());
    var prmVariedad = 0;
    var prmSeccion = 2;

    var parameters = { "prmSemanaINI": prmSemanaINI, "prmSemanaFIN": prmSemanaFIN, "prmUnidadNegocio": prmUnidadNegocio, "prmActividad": prmActividad, "prmProductoVegetal": prmProductoVegetal, "prmVariedad": prmVariedad, "prmSeccion": prmSeccion };

    BlockScreen();
    jQuery.ajax({
        url: "/Planner_Sowing/GetProjection",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery('#ProjectionDataTable').html(data);
            UnlockScreen();
        },
        error: function () {
            UnlockScreen();
        }
    });
}
