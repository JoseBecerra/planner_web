﻿jQuery(document).ready(function () {
  
    $("#lstBusinessUnit").change(function () {
        var intBusinessId = parseInt($("#lstBusinessUnit").find(":selected").val());
        getGreenHousebyBusinessUnit(intBusinessId);
    });

    jQuery(".clickBed").click(function () {
        id = $(this).data('id');
        $("#modalD #BedID").val(id);
    });
});

function getGreenHousebyBusinessUnit(intBusinessId) {
    BlockScreen();

    $("#lstGreenhouse").empty()
    var parameters = { "intBusinessId": intBusinessId };
    jQuery.ajax({
        url: "/Base/getGreenHousebyBusinessUnitId",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.each(data, function (i, data) {
                $("#lstGreenhouse").append('<option value="'
                    + data.ID + '">'
                    + data.Codigo + '</option>');
            });
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
        async: false
    });
}

function onClick() {

    var greenhouseID = parseInt($("#lstGreenhouse").find(":selected").val());

    var values = { "greenhouseID": greenhouseID }

    BlockScreen();
    jQuery.ajax({
        url: "/Planner_Sowing/GetDataSowing",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#visorList').html(data);
            UnlockScreen();
        },
        error: function () {
            UnlockScreen();
        }
    });
}
