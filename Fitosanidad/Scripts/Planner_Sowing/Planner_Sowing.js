﻿jQuery(document).ready(function () {
    jQuery("#startDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
    });
    jQuery('#startDate').datepicker('setDate', 'today');
    jQuery('#startDate').attr("autocomplete", "off");

    jQuery("#endDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1,
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
        weekHeader: "Sem",
    });
    jQuery('#endDate').datepicker('setDate', 'today');
    jQuery('#endDate').attr("autocomplete", "off");

    jQuery("#dataPlanner_Sowing").click(function () {
        jQuery('<table>')
            .append(
                jQuery("#tablePlanner_Sowing").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "Planner_Sowing_Data" //do not include extension
            });
    });
    jQuery("#tablePlanner_Sowing").dataTable();
});

function onClick() {
    var strDate = jQuery("#startDate").val();
    var startDate = new Date(strDate);
    var strDate = jQuery("#endDate").val();
    var endDate = new Date(strDate);

    var values = {"startDate": startDate, "endDate": endDate }

    BlockScreen();
    jQuery.ajax({
        url: "/Planner_Sowing/GetData",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery('#Planner_SowingDataTable').html(data);
            UnlockScreen();
        },
        error: function () {
            UnlockScreen();
        }
    });
}
