﻿jQuery(document).ready(function () {
    $('#Phytosanity_FlatParDataTable').hide();
    $('#Phytosanity_FlatImparDataTable').hide();
    $('#Phytosanity_FlatADataTable').hide();
    $('#Phytosanity_FlatBDataTable').hide();
    $('#Phytosanity_FlatCDataTable').hide();

    $('#Phytosanity_FlatFooterDataTable').hide();

    $("#lstBusinessUnit").change(function () {
        var intBusinessId = parseInt($("#lstBusinessUnit").find(":selected").val());
        getGreenHousebyBusinessUnit(intBusinessId);
    });

});

function CallSubmit() {
    if (!validForm()) {
        return false;
    }

    $('#Phytosanity_FlatParDataTable').hide();
    $('#Phytosanity_FlatImparDataTable').hide();
    $('#Phytosanity_FlatADataTable').hide();
    $('#Phytosanity_FlatBDataTable').hide();
    $('#Phytosanity_FlatCDataTable').hide();

    $('#Phytosanity_FlatFooterDataTable').hide();

    var prmIDSemana = parseInt($("#ddlWeek").find(":selected").val());;
    var prmIDUnidadesNegocios = parseInt($("#lstBusinessUnit").find(":selected").val());;
    var prmIDInvernadero = parseInt($("#lstGreenhouse").find(":selected").val());;

    var parameters = { "prmIDSemana": prmIDSemana, "prmIDUnidadesNegocios": prmIDUnidadesNegocios, "prmIDInvernadero": prmIDInvernadero }

    BlockScreen();
    jQuery.ajax({
        url: "/Phytosanity_Flat/getPhytosanity_FlatHeader",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery('#strWeek').val(data[0]);
            jQuery('#intBlock').val(data[1]);
            jQuery('#intBeds').val(data[2]);
            jQuery('#datSowingDate').val(data[4]);
            jQuery('#strCultivo').val(data[4]);
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });

    BlockScreen();
    jQuery.ajax({
        url: "/Phytosanity_Flat/getPhytosanity_Flat",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data == 1) {
                setPhytosanity_Flat(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 0);
                setPhytosanity_Flat(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 1);
            }
            else {
                setPhytosanity_Flat(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 6);
                setPhytosanity_Flat(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 7);
                setPhytosanity_Flat(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, 8);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });

    BlockScreen();
    jQuery.ajax({
        url: "/Phytosanity_Flat/getPhytosanity_FlatFooter",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#Phytosanity_FlatFooterDataTable').show();

            $('#Phytosanity_FlatFooterDataTable').html(data);
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });
}

function getGreenHousebyBusinessUnit(intBusinessId) {
    BlockScreen();

    $("#lstGreenhouse").empty()
    var parameters = { "intBusinessId": intBusinessId };
    jQuery.ajax({
        url: "/Base/getGreenHousebyBusinessUnitId",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.each(data, function (i, data) {
                $("#lstGreenhouse").append('<option value="'
                    + data.ID + '">'
                    + data.Codigo + '</option>');
            });
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
        async: false
    });
}

function getPdf() {
    if (!validForm()) {
        return false;
    }

    BlockScreen();

    var prmIDSemana = parseInt($("#ddlWeek").find(":selected").val());;
    var prmIDUnidadesNegocios = parseInt($("#lstBusinessUnit").find(":selected").val());;
    var prmIDInvernadero = parseInt($("#lstGreenhouse").find(":selected").val());;

    var parameters = { "prmIDSemana": prmIDSemana, "prmIDUnidadesNegocios": prmIDUnidadesNegocios, "prmIDInvernadero": prmIDInvernadero }

    jQuery.ajax({
        url: "/Phytosanity_Flat/getPhytosanity_FlatPdf",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
        async: false
    });
}

function validForm() {
    if (jQuery('#ddlWeek').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarSemana"))
        jQuery('#ddlWeek').focus();
        return false;
    } else {
        if (jQuery('#ddlWeek').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarSemana"))
            jQuery('#ddlWeek').focus();
            return false;
        };
    }

    if (jQuery('#lstBusinessUnit').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarFinca"))
        jQuery('#lstBusinessUnit').focus();
        return false;
    } else {
        if (jQuery('#lstBusinessUnit').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarFinca"))
            jQuery('#lstBusinessUnit').focus();
            return false;
        };
    }

    if (jQuery('#lstGreenhouse').val() == null) {
        Notifications("error", GetResource("DebeSeleccionarBloque"))
        jQuery('#lstGreenhouse').focus();
        return false;
    } else {
        if (jQuery('#lstGreenhouse').val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarBloque"))
            jQuery('#lstGreenhouse').focus();
            return false;
        };
    }

    return true;
}

function setPhytosanity_Flat(prmIDSemana, prmIDUnidadesNegocios, prmIDInvernadero, prmTipoConsulta) {
    BlockScreen();

    var parameters = {"prmIDSemana": prmIDSemana, "prmIDUnidadesNegocios": prmIDUnidadesNegocios, "prmIDInvernadero": prmIDInvernadero, "prmTipoConsulta": prmTipoConsulta}

    jQuery.ajax({
        url: "/Phytosanity_Flat/setPhytosanity_Flat",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (prmTipoConsulta == 0) {
                $('#Phytosanity_FlatParDataTable').show();
                $('#Phytosanity_FlatParDataTable').html(data);
            }
            if (prmTipoConsulta == 1) {
                $('#Phytosanity_FlatImparDataTable').show();
                $('#Phytosanity_FlatImparDataTable').html(data);
            }

            if (prmTipoConsulta == 6) {
                $('#Phytosanity_FlatADataTable').show();
                $('#Phytosanity_FlatADataTable').html(data);
            }
            if (prmTipoConsulta == 7) {
                $('#Phytosanity_FlatBDataTable').show();
                $('#Phytosanity_FlatBDataTable').html(data);
            }
            if (prmTipoConsulta == 8) {
                $('#Phytosanity_FlatCDataTable').show();
                $('#Phytosanity_FlatCDataTable').html(data);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });
}

function getPlagaColor(strPlaga) {
    BlockScreen();

    var parameters = { "prmIstrPlagaDSemana": strPlaga }


    jQuery.ajax({
        url: "/Phytosanity_Flat/getPlagaColor",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (prmTipoConsulta == 0) {
                $('#Phytosanity_FlatParDataTable').show();
                $('#Phytosanity_FlatParDataTable').html(data);
            }

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });

}