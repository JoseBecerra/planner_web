﻿$(document).ready(function () {
    $("#MyInformationForm").submit(function (e) {
        e.preventDefault();
        MyInformationForm();
        return false;
    });

    $("#ChangepaswordForm").submit(function (e) {
        e.preventDefault();
        ChangepaswordForm();
        return false;
    })
});

function MyInformationForm() {
    var UserId = $("#UserId").val();
    var Login = $("#Login").val();
    var Name = $("#Name").val();
    var LastName = $("#LastName").val();
    var Email = $("#Email").val();
    var DocID = $("#DocID").val();

    var datos = {
        "UserId": UserId,
        "Login": Login,
        "Name": Name,
        "LastName": LastName,
        "Email": Email,
        "DocID": DocID,
    }

    $.ajax({
        dataType: "json",
        type: "post",
        url: "/UserAdministration/MyInformationForm",
        data: datos,
        success: function (r) {
            if (r.Error == 0) {
                Notifications("success", r.Mensaje);
            }
        }
    })
}

function ChangepaswordForm() {
    var Pass = $("#Pass").val();
    var PassConfirm = $("#PassConfirm").val();
    var UserId = $("#UserId").val();

    if (Pass != PassConfirm) {
        Notifications("error", "Contraseñas no coinciden, verifique!");
        return false;
    }

    var datos = {
        "Password": Pass,
        "UserId": UserId
    };

    $.ajax({
        DataType: "ajax",
        type: "post",
        url: "/UserAdministration/ChangepaswordForm",
        data: datos,
        success: function (r) {
            if (r.Error == 0) {
                Notifications("success", r.Mensaje);
                $("#Pass").val("");
                $("#PassConfirm").val("");
            }
        }
    });
}
