﻿jQuery(document).ready(function () {
});

function BackToList() {
    var Url = "/Planner_Template";
    window.location.href = Url;
}

function CallSubmit() {
    if (jQuery('#Name').val() == null) {
        Notifications("error", GetResource("DebeDigitarNombre"))
        jQuery('#Name').focus();
        return false;
    } else {
        if (jQuery('#Name').val().length == 0) {
            Notifications("error", GetResource("DebeDigitarNombre"))
            jQuery('#Name').focus();
            return false;
        };
    }

    if (jQuery("#Description").val() == '') {
        Notifications("error", GetResource("DebeDigitarDescripcion"))
        jQuery('#Description').focus();
        return false;
    } else {
        if (jQuery("#Description").val().length == 0) {
            Notifications("error", GetResource("DebeDigitarDescripcion"))
            jQuery('#Description').focus();
            return false;
        };
    }

    if (jQuery("#lstPlanner_TemplateType").val() == '') {
        Notifications("error", GetResource("DebeSeleccionarTipo"))
        jQuery('#lstPlanner_TemplateType').focus();
        return false;
    } else {
        if (jQuery("#lstPlanner_TemplateType").val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarTipo"))
            jQuery('#lstPlanner_TemplateType').focus();
            return false;
        };
    }

    var Name = jQuery("#Name").val();
    var Description = jQuery("#Description").val();
    var TemplateTypeID = jQuery("#lstPlanner_TemplateType").val();
    var ActiveFlag = jQuery("#ActiveFlag").val();

    var parameters = { "Name": Name, "Description": Description, "TemplateTypeID": TemplateTypeID, "ActiveFlag": ActiveFlag }

    BlockScreen();
    jQuery.ajax({
        url: "/Planner_Template/Planner_TemplateCreate",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", errorThrown)
        },
    });
}