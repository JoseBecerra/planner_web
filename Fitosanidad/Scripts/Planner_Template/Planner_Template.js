﻿jQuery(document).ready(function () {

    jQuery("#btnCreate").click(function () {
        CreateRow();
    });

    jQuery("#dataPlanner_Template").click(function () {
        jQuery('<table>')
            .append(
                jQuery("#tablePlanner_Template").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "Planner_Template_Data" //do not include extension
            });
    });
    jQuery("#tablePlanner_Template").dataTable();
});

function CreateRow() {
    var url = "/Planner_Template/Create";
    window.location = url;
}

function EditRow(TemplateID) {
    var url = "/Planner_Template/Edit?_TemplateID=" + TemplateID;

    window.location = url;
}

function DelRow(IOT_SensorListID) {
    var values = { "_IOT_SensorListID": IOT_SensorListID };
    jQuery.ajax({
        url: "/IOT_SensorList/GetIOT_SensorListByIOT_SensorListID",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery("#IOT_SensorListID").val(data.IOT_SensorListID);

            jQuery('#DelbusinessUnitList').val(data.BusinessUnitID);
            getgreenhouseListByBusinessUnitId('#DelgreenhouseList', data.BusinessUnitID, data.GreenHouseID);
            jQuery('#DelgreenhouseList').val(data.GreenHouseID);

            jQuery('#DelName').val(data.Name);
            jQuery('#DelService').val(data.Service);
            jQuery('#DelExternalID').val(data.ExternalID);
            jQuery("#DelFlagActive").prop('checked', data.FlagActive);
        },
        error: function () {
        }
    });
    jQuery("#modDelete").modal('show');
}
