﻿jQuery(document).ready(function () {

    jQuery("#dataPlanner_TemplateDetail").click(function () {
        jQuery('<table>')
            .append(
                jQuery("#tablePlanner_TemplateDetail").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "Planner_TemplateDetail_Data" //do not include extension
            });
    });
    jQuery("#tablePlanner_TemplateDetail").dataTable();
});

function BackToList() {
    var Url = "/Planner_Template";
    window.location.href = Url;
}

function CallSubmit() {
    if (jQuery('#Name').val() == null) {
        Notifications("error", GetResource("DebeDigitarNombre"))
        jQuery('#Name').focus();
        return false;
    } else {
        if (jQuery('#Name').val().length == 0) {
            Notifications("error", GetResource("DebeDigitarNombre"))
            jQuery('#Name').focus();
            return false;
        };
    }

    if (jQuery("#Description").val() == '') {
        Notifications("error", GetResource("DebeDigitarDescripcion"))
        jQuery('#Description').focus();
        return false;
    } else {
        if (jQuery("#Description").val().length == 0) {
            Notifications("error", GetResource("DebeDigitarDescripcion"))
            jQuery('#Description').focus();
            return false;
        };
    }

    if (jQuery("#lstPlanner_TemplateType").val() == '') {
        Notifications("error", GetResource("DebeSeleccionarTipo"))
        jQuery('#lstPlanner_TemplateType').focus();
        return false;
    } else {
        if (jQuery("#lstPlanner_TemplateType").val().length == 0) {
            Notifications("error", GetResource("DebeSeleccionarTipo"))
            jQuery('#lstPlanner_TemplateType').focus();
            return false;
        };
    }

    jQuery("#Edit").submit();
}