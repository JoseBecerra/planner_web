﻿$(document).ready(function () {

    traeID();
    pintaBloque();
    TablaMaestra();

    $("#form_AspersionPlanDetail").submit(function (e) {
        e.preventDefault;
        var contador = 0;
        var arr = [];
        $("#PPC_div").find("select").each(function (i) {
            if (this.value) {
                contador++;
                arr.push(this.value);
            }
        });

        if (contador == 0) {
            Notifications("error", "Debe seleccionar al menos un PPC verifique!");
            location = "#divNotifications";
            return false;
        }
        //guardadoGeneral(JSON.stringify(arr));
        guardadoGeneral(arr);

        return false;
    });

    $("#LiterPerBed").keypress(function (event) {
        return soloNumerosDecimal(event, this);
    });

    $("#PreparationOrder").keypress(function (event) {
        return soloNumeros(event);
    });

    $("#BedNumber").keypress(function (event) {
        return soloNumeros(event);
    });

    $("#AsperjadorNumber").keypress(function (event) {
        return soloNumeros(event);
    });

    $("#TankVolume").keypress(function (event) {
        return soloNumeros(event);
    });

    $("#CapacityJar").keypress(function (event) {
        return soloNumerosDecimal(event, this);
    });
});

function TablaMaestra() {
    var AspersionPlanID = $("#AspersionPlanID").val();
    var datos = {
        "AspersionPlanID": AspersionPlanID,
    }
    $.ajax({
        type: "POST",
        DataType: "json",
        data: JSON.stringify(datos),
        url: "/AspersionPlan/AspersionPlanListDetail",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            $("#AspersionPlanListDetail").html(r);
            table.dataTable({
                aoColumns: [
                    { "sWidth": "50px" },
                ],
                "columnDefs": [
                    {
                        "targets": [16],
                        "visible": false,
                        "searchable": false
                    }
                ],
                "bDestroy": true,
                "columns": [

                ]
            });
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });

}

function BackToList() {
    location = "/AspersionPlan/";
}

function pintaBloque() {
    var Id = $("#GreenHouseID").val();
    var datos = {
        "Id": Id,
    }
    $.ajax({
        type: "POST",
        data: datos,
        DataType: "json",
        url: "/AspersionPlan/FiltroBloques?IdCompany=" + parseInt(Id),
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            var html = "<option value=''>Seleccione</option>";
            for (x in r) {
                html += "<option value='" + r[x].Value + "'>" + r[x].Text + "</option>";
            }
            $("#Block").html(html);
            $("#Block").html(html);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });

}

function traeID() {
    var WeekID = $("#WeekID").val();
    var GreenHouseID = $("#GreenHouseID").val();
    if (!WeekID || !GreenHouseID) {
        bloqueaGuardado();
        return false;
    }

    var datos = {
        "WeekID": WeekID,
        "GreenHouseID": GreenHouseID,
    };

    $.ajax({
        type: "POST",
        data: JSON.stringify(datos),
        DataType: "json",
        url: "/AspersionPlan/TraeID",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            if (r.Ctrl == 2) {
                $("#form_AspersionPlanDetail").show();
                $("#AspersionPlanID").val(r);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });
}

function guardadoGeneral(AgrochemicalProductsID)
{
    if (!AgrochemicalProductsID) {
        console.log("No se envió producto agroquímico");
        return false;
    }
    var AspersionPlanID = $("#AspersionPlanID").val();
    var ProductID = $("#ProductID").val();
    var Block = $("#Block").val();
    var DayNumber = $("#DayNumber").val();
    var ApplyTypesID = $("#ApplyTypesID").val();
    var FormID = $("#FormID").val();
    var PreparationOrder = $("#PreparationOrder").val();
    var AspersionEmployeesID = $("#AspersionEmployeesID").val();
    var BedNumber = $("#BedNumber").val();
    var LiterPerBed = $("#LiterPerBed").val();
    var ReferenceSpear = $("#ReferenceSpear").val();
    var AsperjadorNumber = $("#AsperjadorNumber").val();
    var Observation = $("#Observation").val();
    var TankVolume = $("#TankVolume").val();
    var CapacityJar = $("#CapacityJar").val();
    var ActiveFlag = true;

    LiterPerBed = parseFloat(LiterPerBed);
    TankVolume = parseFloat(TankVolume);

    if (AspersionPlanID === "0") {
        Notifications("error", "Debe crear el plan, no existe, Verifique!");
        return false;
    }

    var datos = {
        "AspersionPlanID": AspersionPlanID,
        "ProductID": ProductID,
        "Block": Block,
        "DayNumber": DayNumber,
        "ApplyTypesID": ApplyTypesID,
        "FormID": FormID,
        "PreparationOrder": PreparationOrder,
        "AgrochemicalProductsID": AgrochemicalProductsID,
        "BedNumber": BedNumber,
        "LiterPerBed": LiterPerBed,
        "ReferenceSpear": ReferenceSpear,
        "AsperjadorNumber": AsperjadorNumber,
        "Observation": Observation,
        "TankVolume": TankVolume,
        "CapacityJar": CapacityJar,
        "ActiveFlag": ActiveFlag,
        "d": AgrochemicalProductsID,
        "AspersionEmployeesID": AspersionEmployeesID
    }

    $.ajax({
        type: "POST",
        DataType: "json",
        data: JSON.stringify(datos),
        url: "/AspersionPlan/AspersionPlanAddDetail",
        cache: false,
        contentType: 'application/json; charset=utf-8',
        async : false,
        success: function (r) {
            if (r === "Ok") {
                Notifications("success", "Registro insertado correctamente");
                $("#Block").val("");
                $("#BedNumber").val("");
            }

            if (r.Error == 1) {
                Notifications("error", r.Mensaje);
            }
            TablaMaestra();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });
}

function ProductByBlock(BlockId) {
    if (!BlockId) {
        $("#ProductID").val("");
        return false;
    }
    $.ajax({
        dataType: "json",
        type: "post",
        url: "/AspersionPlan/ProductByBlock",
        data: { "BlockId": BlockId },
        success: function (r) {
            var html = "";
            for(x in r)
            {
                html += "<option value='" + r[x]['Value'] + "'>" + r[x]['Text'] + "</option>";
            }
            $("#ProductID").html(html);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    })
}