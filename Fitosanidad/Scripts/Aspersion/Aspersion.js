﻿jQuery(document).ready(function () {
    TablaMaestra();

    $("#form_AgrochemicalProducts").submit(function (e) {
        e.preventDefault();

        var Name = $("#Name").val();
        var ChemicalGroupID = $("#ChemicalGroupID").val();
        var FracID = $("#FracID").val();
        var ActiveIngredientsID = $("#ActiveIngredientsID").val();
        var ToxicologicalCategoryID = $("#ToxicologicalCategoryID").val();
        var ReentryHourID = $("#ReentryHourID").val();
        var ProductOrigin = $("#ProductOrigin").val();
        var Aspersion = $("#Aspersion").val();
        var Sprinkle = $("#Sprinkle").val();
        var Drench = $("#Drench").val();
        var Thermonebulizer = $("#Thermonebulizer").val();
        var Density = $("#Density").val();
        var PlagasID = $("#PlagasID").val();
        var MeasurementUnitsID = $("#MeasurementUnitsID").val();

        Aspersion = parseFloat(Aspersion);
        Sprinkle = parseFloat(Sprinkle);
        Drench = parseFloat(Drench);
        Thermonebulizer = parseFloat(Thermonebulizer);
        Density = parseFloat(Density);

        var datos = {
            "Edita": 0,
            "Name": Name.toUpperCase(),
            "ChemicalGroupID": ChemicalGroupID,
            "FracID": FracID,
            "ActiveIngredientsID": ActiveIngredientsID,
            "ToxicologicalCategoryID": ToxicologicalCategoryID,
            "ReentryHourID": ReentryHourID.toUpperCase(),
            "ProductOrigin": ProductOrigin,
            "Aspersion": Aspersion,
            "Sprinkle": Sprinkle,
            "Drench": Drench,
            "Thermonebulizer": Thermonebulizer,
            "Density": Density,
            "PlagasID": PlagasID,
            "MeasurementUnitsID": MeasurementUnitsID,
        };
        $.ajax({
            type: "POST",
            DataType: "json",
            data: JSON.stringify(datos),
            url: "/AgrochemicalProducts/Crear?Edita=0",
            cache: false,
            contentType: 'application/json; charset=utf-8',
            success: function (r) {
                $('#modalMensajes .modal-header').addClass("bg-green");
                $("#modalMensajesTitulo").html("<span class='text-green'>Mensaje</span>");
                $('#modalMensajes').modal('show');
                $("#modalMensajesCuerpo").html("Registro adicionado correctamente");
                $('#form_AgrochemicalProducts')[0].reset();

                TablaMaestra();
                setTimeout(function () { $("#TableAgrochemicalProducts").dataTable(); }, 100);
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });
        return false;
    });


    $("#formEdit_AgrochemicalProducts").submit(function (e) {
        e.preventDefault;
        var Name = $("#Name").val();
        var ChemicalGroupID = $("#ChemicalGroupID").val();
        var FracID = $("#FracID").val();
        var ActiveIngredientsID = $("#ActiveIngredientsID").val();
        var ToxicologicalCategoryID = $("#ToxicologicalCategoryID").val();
        var ReentryHourID = $("#ReentryHourID").val();
        var ProductOrigin = $("#ProductOrigin").val();
        var Aspersion = $("#Aspersion").val();
        var Sprinkle = $("#Sprinkle").val();
        var Drench = $("#Drench").val();
        var Thermonebulizer = $("#Thermonebulizer").val();
        var Density = $("#Density").val();
        var PlagasID = $("#PlagasID").val();
        var MeasurementUnitsID = $("#MeasurementUnitsID").val();
        var AgrochemicalProductsID = $("#AgrochemicalProductsID").val();

        Aspersion = parseFloat(Aspersion);
        Sprinkle = parseFloat(Sprinkle);
        Drench = parseFloat(Drench);
        Thermonebulizer = parseFloat(Thermonebulizer);
        Density = parseFloat(Density);

        var datos = {
            "Name": Name.toUpperCase(),
            "ChemicalGroupID": ChemicalGroupID,
            "FracID": FracID,
            "ActiveIngredientsID": ActiveIngredientsID,
            "ToxicologicalCategoryID": ToxicologicalCategoryID,
            "ReentryHourID": ReentryHourID.toUpperCase(),
            "ProductOrigin": ProductOrigin,
            "Aspersion": Aspersion,
            "Sprinkle": Sprinkle,
            "Drench": Drench,
            "Thermonebulizer": Thermonebulizer,
            "Density": Density,
            "PlagasID": PlagasID,
            "MeasurementUnitsID": MeasurementUnitsID,
            "AgrochemicalProductsID": AgrochemicalProductsID,
        };
        console.log(datos);
        //return false;
        $.ajax({
            type: "POST",
            DataType: "json",
            data: JSON.stringify(datos),
            url: "/AgrochemicalProducts/Crear?Edita=1",
            cache: false,
            contentType: 'application/json; charset=utf-8',
            success: function (r)
            {
                $('#modalMensajes .modal-header').addClass("bg-green");
                $("#modalMensajesTitulo").html("Mensaje");
                $('#modalMensajes').modal('show');
                $("#modalMensajesCuerpo").html("Registro actualizado correctamente");
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });
        //window.location = "/AgrochemicalProducts/Index";
        return false;
    });

    $("#ProductOrigin").change(function () {
        var texto = $("#ProductOrigin option:selected").text();
        $("#Name").val(texto.trim()).focus();
    })

});

function TablaMaestra() {
    $.ajax({
        type: "POST",
        DataType: "json",
        url: "/AgrochemicalProducts/DetalleMaestro",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            $("#DetalleMaestro").html(r);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });
}

function CambiaEstado(estado, id)
{
    console.log(estado);
    var ActiveFlag = null;
    if (estado === 0)
        ActiveFlag = true;
    else
        ActiveFlag = false;
    var datos = {
        "ActiveFlag": ActiveFlag,
        "AgrochemicalProductsID": id,
    };
    $.ajax({
        type: "POST",
        DataType: "json",
        data: JSON.stringify(datos),
        url: "/AgrochemicalProducts/CambiaEstado",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            //$("#DetalleMaestro").html(r);
            TablaMaestra();
            console.log("Cambio Estado OK");
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });
}

function BackToList() {
    location = "/AgrochemicalProducts/";
}