﻿$(document).ready(function () {
    $("#btnCreate").on("click", function () {
        location = "/AspersionPlan/AspersionPlanAdd";
    });

    $("#bt_AspersionPlanAdd").click(function () {
        var WeekID = $("#WeekID").val();
        var GreenHouseID = $("#GreenHouseID").val();
        console.log(GreenHouseID);
    });

    $("#AspersioPlanOutputTable").click(function () {
        location = "/AspersionPlan/AspersioPlanOutputTable";
        return false;
    });
});

function CambiaEstado(estado, id) {
    var ActiveFlag = null;
    if (estado === 0)
        ActiveFlag = true;
    else
        ActiveFlag = false;
    var datos = {
        "ActiveFlag": ActiveFlag,
        "AspersionPlanID": id,
    };
    $.ajax({
        type: "POST",
        DataType: "json",
        data: JSON.stringify(datos),
        url: "/AspersionPlan/CambiaEstadoHead",
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            location.reload();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        },
    });
}


