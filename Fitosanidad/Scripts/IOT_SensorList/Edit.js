﻿jQuery(document).ready(function () {
    jQuery("#ddlGreenhouse").empty();
    jQuery("#ddlGreenhouse").append(jQuery("<option></option>").val('0').html('EXTERIOR - Cód. 0'));

    jQuery("#ddlBusinessUnit").change(function () {
        var businessUnitID = jQuery("#ddlBusinessUnit").find(":selected").val();
        var values = { "businessUnitID": businessUnitID }
        $("#ddlGreenhouse").empty()

        BlockScreen();
        jQuery.ajax({
            url: "/IOT_SensorList/greenhouseListByBusinessUnitID",
            data: JSON.stringify(values),
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $.each(data, function (i, data) {
                    $("#ddlGreenhouse").append('<option value="'
                        + data.Value + '">'
                        + data.Text + '</option>');
                });
                jQuery("#ddlGreenhouse").append(jQuery("<option></option>").val('0').html('EXTERIOR - Cód. 0'));
                UnlockScreen();
            },
            error: function (xhr, textStatus, errorThrown) {
                UnlockScreen();
                Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#lstBusinessUnit")).text());
            },
        });
    });
});

function BackToList() {
    var Url = "/IOT_SensorList";
    window.location.href = Url;
}

function greenhouseListOnChange() {
    var businessUnitID = jQuery("#ddlGreenhouse").find(":selected").val();

    var parameters = { "businessUnitID": businessUnitID };

    $("#ddlDetails").empty();

    BlockScreen();
    jQuery.ajax({
        url: "/IOT_SensorList/greenhouseListByBusinessUnitID",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $("#ddlDetails").append('<option value="">' + GetResource("Seleccionar") + '</option>');

            $.each(data, function (i, data) {
                $("#ddlDetails").append('<option value="'
                    + data.Value + '">'
                    + data.Text + '</option>');
            });

            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            UnlockScreen();
            Notifications("error", GetResource("NoSeEncontraronDatos") + " " + GetResource("Para").toLowerCase() + ": " + $("option:selected", $("#lstBusinessUnit")).text());
        },
        async: false
    });
}

function CallSubmit() {
    if (jQuery("#Name").val() == "") {
        jQuery('#Name').focus();
        return false;
    }
    if (jQuery("#Service").val() == "") {
        jQuery('#Service').focus();
        return false;
    }
    if (jQuery("#ExternalID").val() == "") {
        jQuery('#ExternalID').focus();
        return false;
    }

    var IOT_SensorListID = jQuery("#IOT_SensorListID").val();
    var businessUnitId = jQuery("#ddlBusinessUnit").find(":selected").val();
    var greenHouseId = jQuery("#ddlGreenhouse").find(":selected").val();
    var name = jQuery("#Name").val();
    var service = jQuery("#Service").val();
    var flagActive = jQuery("#FlagActive").is(":checked");
    var externalId = jQuery("#ExternalID").val();
    var CRUD = "U";
    var values = { "IOT_SensorListID": IOT_SensorListID, "businessUnitId": businessUnitId, "greenHouseId": greenHouseId, "name": name, "service": service, "flagActive": flagActive, "externalId": externalId, "CRUD": CRUD };

    BlockScreen();
    jQuery.ajax({
        url: "/IOT_SensorList/IOT_SensorList_SaveChanges",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery("#Name").val("");
            jQuery("#Service").val("");
            jQuery("#ExternalID").val("");
            jQuery("#FlagActive").attr('checked', false);

            UnlockScreen();
            BackToList();
        },
        error: function () {
            UnlockScreen();
        }
    });
};