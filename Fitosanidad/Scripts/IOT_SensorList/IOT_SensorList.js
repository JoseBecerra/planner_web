﻿jQuery(document).ready(function () {
    jQuery("#dataSensor").click(function () {
        jQuery('<table>')
            .append(
                jQuery("#tableSensor").DataTable().$('tr').clone()
            )
            .table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "Sensor_Data" //do not include extension
            });
    });
    jQuery("#tableSensor").dataTable();

    $("#btnCreate").click(function () {
        CreateRow();
    });
});

function CreateRow() {
    var url = "/IOT_SensorList/Create";
    window.location = url;
}

function EditRow(arrKey) {
    var ID = arrKey[0];
    var url = "/IOT_SensorList/Edit?ID=" + ID;

    window.location = url;
}

function DelRow(arrKey) {
    var ID = arrKey[0];
    var url = "/IOT_SensorList/Delete?ID=" + ID;

    window.location = url;
}
